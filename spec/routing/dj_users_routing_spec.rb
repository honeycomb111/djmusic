require "rails_helper"

RSpec.describe DjUsersController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/dj_users").to route_to("dj_users#index")
    end

    it "routes to #new" do
      expect(:get => "/dj_users/new").to route_to("dj_users#new")
    end

    it "routes to #show" do
      expect(:get => "/dj_users/1").to route_to("dj_users#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/dj_users/1/edit").to route_to("dj_users#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/dj_users").to route_to("dj_users#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/dj_users/1").to route_to("dj_users#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/dj_users/1").to route_to("dj_users#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/dj_users/1").to route_to("dj_users#destroy", :id => "1")
    end

  end
end
