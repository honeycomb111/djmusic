# == Schema Information
#
# Table name: cover_photos
#
#  id                  :integer          not null, primary key
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  source_file_name    :string
#  source_content_type :string
#  source_file_size    :integer
#  source_updated_at   :datetime
#  xOffset             :integer          default(0)
#  yOffset             :integer          default(0)
#  Float               :integer          default(0)
#  webLink             :string
#

#  source_file_name    :string
#  source_content_type :string
#  source_file_size    :integer
#  source_updated_at   :datetime
#

FactoryGirl.define do
  factory :cover_photo do
    xOffset 1.5
    yOffset "MyString"
    Float "MyString"
    source "MyString"
  end
end
