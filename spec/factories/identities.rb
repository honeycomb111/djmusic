# == Schema Information
#
# Table name: identities
#
#  id           :integer          not null, primary key
#  fb_id        :string
#  access_token :string
#  user_id      :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryGirl.define do
  factory :identity do
    fb_id "MyString"
    access_token "MyString"
  end
end
