# == Schema Information
#
# Table name: reviews
#
#  id         :integer          not null, primary key
#  content    :string
#  stars      :integer
#  dj_user_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Indexes
#
#  index_reviews_on_dj_user_id  (dj_user_id)
#

FactoryGirl.define do
  factory :review do
    content ""
    stars ""
    dj_users nil
  end
end
