# == Schema Information
#
# Table name: music_types
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :music_type do
    name "MyString"
    description "MyText"
  end
end
