# == Schema Information
#
# Table name: event_music_relations
#
#  id            :integer          not null, primary key
#  event_id      :integer
#  music_type_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_event_music_relations_on_event_id                    (event_id)
#  index_event_music_relations_on_event_id_and_music_type_id  (event_id,music_type_id) UNIQUE
#  index_event_music_relations_on_music_type_id               (music_type_id)
#

FactoryGirl.define do
  factory :event_music_relation do
    
  end
end
