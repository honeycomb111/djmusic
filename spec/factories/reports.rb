# == Schema Information
#
# Table name: reports
#
#  id         :integer          not null, primary key
#  dj_user_id :integer
#  period     :string
#  gigs       :integer          default(0)
#  likes      :integer          default(0)
#  tips       :integer          default(0)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_reports_on_dj_user_id  (dj_user_id)
#

FactoryGirl.define do
  factory :report do
    
  end
end
