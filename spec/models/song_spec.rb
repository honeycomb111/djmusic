# == Schema Information
#
# Table name: songs
#
#  id          :integer          not null, primary key
#  name        :string
#  artist_name :string
#  event_id    :integer
#  dj_user_id  :integer
#  user_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  status      :integer          default(0)
#
# Indexes
#
#  index_songs_on_dj_user_id                           (dj_user_id)
#  index_songs_on_event_id                             (event_id)
#  index_songs_on_event_id_and_dj_user_id_and_user_id  (event_id,dj_user_id,user_id)
#  index_songs_on_user_id                              (user_id)
#

require 'rails_helper'

RSpec.describe Song, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
