# == Schema Information
#
# Table name: bars
#
#  id                       :integer          not null, primary key
#  rating                   :integer
#  price                    :string
#  phone                    :string
#  is_closed                :boolean
#  categories_alias         :string
#  categories_title         :string
#  review_count             :integer
#  name                     :string
#  url                      :string
#  lat                      :float
#  long                     :float
#  image_url                :string
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  bar_id                   :string
#  display_phone            :string
#  address1                 :string
#  address2                 :string
#  address3                 :string
#  city                     :string
#  zip_code                 :string
#  country                  :string
#  state                    :string
#  display_address          :string
#  city_id                  :integer
#  hours_type               :string
#  is_overnight             :boolean
#  bar_end                  :string
#  day                      :string
#  start                    :string
#  is_credits_cards         :boolean
#  is_apple_pay             :boolean
#  is_android_pay           :boolean
#  is_bike_parking          :boolean
#  is_wheelchair_accessible :boolean
#  is_good_for_groups       :boolean
#  is_good_for_dancing      :boolean
#  happy_hour               :string
#  parking                  :string
#  ambience                 :string
#  noise_level              :string
#  music                    :string
#  alchole                  :string
#  best_nights              :string
#  age_allowed              :string
#
# Indexes
#
#  index_bars_on_bar_id_and_lat_and_long  (bar_id,lat,long)
#  index_bars_on_city_id                  (city_id)
#
# Foreign Keys
#
#  fk_rails_...  (city_id => cities.id)
#

require 'rails_helper'

RSpec.describe Bar, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
