# == Schema Information
#
# Table name: bar_vibe_relations
#
#  id           :integer          not null, primary key
#  bar_id       :integer
#  vibe_type_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_bar_vibe_relations_on_bar_id                   (bar_id)
#  index_bar_vibe_relations_on_bar_id_and_vibe_type_id  (bar_id,vibe_type_id) UNIQUE
#  index_bar_vibe_relations_on_vibe_type_id             (vibe_type_id)
#
# Foreign Keys
#
#  fk_rails_...  (bar_id => bars.id)
#  fk_rails_...  (vibe_type_id => vibe_types.id)
#

require 'rails_helper'

RSpec.describe BarVibeRelation, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
