namespace :bars do
  desc "Get all bars"
  task :get_bars => :environment do
    begin
      BARS_URL = "https://api.yelp.com/v3/businesses/search"
      AUTH_URL = "https://api.yelp.com/oauth2/token"


      ret = RestClient.post AUTH_URL, {:client_id => Rails.application.secrets.yelp_id, :client_secret => Rails.application.secrets.yelp_secret}


      token = JSON.parse(ret)["access_token"]

      cities = City.all
      if cities.present?
        headers = { 
          "Authorization" => "Bearer #{token}"
        }
        
      
        cities.each do |city|
          offset = 1
          total = 51
          while(offset <= total)
            yel_url = "https://api.yelp.com/v3/businesses/search?&location=#{city.name}&categories=bars&limit=50&offset=#{offset}"

            bars_response = HTTParty.get(yel_url,:headers => headers)
            total = bars_response["total"].to_i if bars_response["total"].to_i > 50
            
            begin 
              bars_response["businesses"].each do |bar|
                @cat_alias, @cat_title = [], []
                if bar["categories"].present?
                  bar["categories"].each do |cat|
                    @cat_alias << cat["alias"].delete('\\"')
                    @cat_title << cat["title"].delete('\\"')
                  end
                end
  
                yelp_business_url = "https://api.yelp.com/v3/businesses/#{bar['id']}"
                bar_bus_response = HTTParty.get(yelp_business_url,:headers => headers)
                
                if bar_bus_response["hours"].present?
                  bar_bus_response["hours"].each do |hrs|
                    @is_overnight, @end, @start , @day = [], [], [], []
                    @hours_type =  hrs["hours_type"]
                    hrs["open"].each do |hr|
                      @is_overnight << hr["is_overnight"]
                      @end << hr["end"]
                      @day << hr["day"]
                      @start << hr["start"]
                    end
                  end
                end
  
                barrr = Bar.find_by_bar_id_and_lat_and_long(bar["id"], bar["coordinates"]["latitude"], bar["coordinates"]["longitude"])
                if barrr.present?
                  barrr.update_attributes(bar_id: bar["id"], rating: bar["rating"], price:bar["price"] , phone: bar["phone"],  is_closed: bar["is_closed"], categories_alias: @cat_alias, categories_title: @cat_title, review_count: bar["review_count"],  name: bar["name"] , url: bar["url"],  lat: bar["coordinates"]["latitude"],  long: bar["coordinates"]["longitude"],  image_url: bar["image_url"], display_phone: bar["display_phone"], address1: bar["location"]["address1"],address2: bar["location"]["address2"],address3: bar["location"]["address3"], display_address:bar["location"]["display_address"],  zip_code: bar["location"]["zip_code"], country: bar["location"]["country"], state: bar["location"]["state"], hours_type: @hours_type , is_overnight: @is_overnight, bar_end: @end, day: @day,start: @start)
                else
                  bar1 =  Bar.new(bar_id: bar["id"], rating: bar["rating"], price:bar["price"] , phone: bar["phone"],  is_closed: bar["is_closed"], categories_alias: @cat_alias, categories_title: @cat_title, review_count: bar["review_count"],  name: bar["name"] , url: bar["url"],  lat: bar["coordinates"]["latitude"],  long: bar["coordinates"]["longitude"],  image_url: bar["image_url"], display_phone: bar["display_phone"], address1: bar["location"]["address1"], address2: bar["location"]["address2"],address3: bar["location"]["address3"], city: city, display_address:bar["location"]["display_address"], zip_code: bar["location"]["zip_code"], country: bar["location"]["country"], state: bar["location"]["state"], hours_type: @hours_type , is_overnight: @is_overnight, bar_end: @end, day: @day,start: @start)
                  bar1[:city]  =  bar["location"]["city"]
                  bar1.save!
                  city.bars << bar1
                end
              end
            rescue StandardError => e
            # print e
            end
            offset+=50            
          end
        end
        puts "Bars added successfully!"

      end
    rescue Exception => exc 
      puts "Something went wrong, Please check it! #{exc.message}"
    end
  end
end