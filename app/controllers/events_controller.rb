# # class Api::V1::EventsController < Api::V1::BaseController
# class EventsController < ApplicationController
#   before_action :verify_token
#   before_action :current_user, only: [:fb_events, :get_favorite]
#   before_action :set_event, only: [:show, :edit, :update, :destroy]

#   # GET /events
#   def index
#     @events = Event.all
#     render :status => 200,
#             :json => { :success => true,
#                         :info => "All Event Information",
#                         :data => {:events => @events.as_json },:status => 200 }
#   end

#   # GET /events/1
#   def show
#     if @event.blank?
#       render :status => 400,
#           :json => { :success => false,
#                       :info => "Event Not Found",
#                       :data => {  } }
#     else
#       render :status => 200,
#           :json => { :success => true,
#                       :info => "Event Information",
#                       :data => { :event => @event.first.as_json } }
#     end
#   end

#   # POST /events
#   def create
#     cover_photo = CoverPhoto.new(cover_photo_params)
#     cover_photo.build_event(event_params)
#     if cover_photo.save
#       update_gigs
     
#       @event = cover_photo.event
#       render :status => 200,
#               :json => { :success => true,
#                           :info => "#{@event.class} created Successfully",
#                           :data => {:event => @event },:status => 200, }
#     else
#       render :status => 400,:json => { :success => false,:info => @event.errors.full_messages.join(", "),:data => {} }
#     end
#   end

#   # PATCH/PUT /events/1
#   def update
#     if @event.blank?
#       render :status => 400,
#           :json => { :success => false,
#                       :info => "Event Not Found",
#                       :data => {  } }
#     else
#       @event = @event.first
#       if @event.update(event_params) && @event.cover_photo.update(cover_photo_params)
#       # if @event.update(event_params)
#         render :status => 200,
#               :json => { :success => true,
#                           :info => 'Event was successfully updated.',
#                           :data => {:event => @event },:status => 200, }
#       else
#         render :status => 400,:json => { :success => false,:info => @event.errors.full_messages.join(", "),:data => {} }
#       end
#     end
#   end

#   # DELETE /events/1
#   def destroy
#     if @event.blank?
#       render :status => 400,
#           :json => { :success => false,
#                       :info => "Event Not Found",
#                       :data => {  } }
#     else
#       @event = @event.first
#       @event.destroy
#       render :status => 200,
#             :json => { :success => true,
#                         :info => 'Event successfully destroyed.',
#                         :data => {  } }
#     end
#   end
#   def fb_events
#     if @current_user
#       begin
#         if @current_user.identity
#           client = Koala::Facebook::API.new @current_user.identity.access_token
#           events = client.get_object('me/events')
#           render :status => 200,
#                 :json => { :success => true,
#                             :info => 'Fb events list.',
#                             :data => { :fb_events => events } }
#         else
#           render :status => 400,
#                 :json => {:success => false,
#                           :info => "Fb token not present", :data => {}}
#         end
#       rescue Exception => ex
#         render :status => 400,
#               :json => {:success => false,
#                         :info => "#{ex.message}", :data => {}}
#       end
#     else
#       render :status => 400,
#             :json => {:success => false,
#                       :info => "User Not Found",
#                       :data => {}}
#     end
#   end
  
#   def get_favorite
#   render :status => 200,
#     :json => { :success => true,
#                 :info => 'favorite events list',
#                 :data => { :favorite =>  @current_user.follows_by_type('Event') } }
   
#   end
#   private
#     # Use callbacks to share common setup or constraints between actions.
#     def set_event
#       @event = Event.where(id: params[:id])
#     end

#     # Only allow a trusted parameter "white list" through.
#     def event_params
#       params.require(:event).permit(:title, :longDescription, :shortDescription, :hostedBy, :latitude, :longtitude, :restrictAge, :openingTime, :endTime, :attendingCount, :interestedCount, :isCanceled, :isFree, :fbLink, :city_id, :music_type_id, :vibe_type_id, :dj_type_id, :cover_photo_id, :dj_user_id)
#     end

#     def cover_photo_params
#       params.require(:event).permit(:xOffset, :yOffset, :Float, :source)
#     end
    
#     def update_gigs
#       if event_params.openingTime.nil? == false 
#         event_period = params[:openingTime].strftime("%Y-%d")
#         report = Report.where(dj_user_id: params[:dj_user_id]).where(period:event_period)
#         if report.nil? 
#           Report.create(dj_user_id: params[:dj_user_id], period: event_period, gigs: 1)
#         else
#           report.gigs += 1
#           report.save
#         end
#       end
      
#     end
# end
