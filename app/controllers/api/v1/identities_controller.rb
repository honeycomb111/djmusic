class Api::V1::IdentitiesController < Api::V1::BaseController
  before_action :verify_token
  before_action :current_user, only: :create

  def_param_group :identity do
    param :authentication_token, String, "Authentication Token", required: true
    param :fb_id, Integer, "Facebook user id"
    param :access_token, String, "Access Token"
  end

  api :POST, '/identities', "Create identities"
  param_group :identity
  error :code => 404, :desc => "User Not Found", :meta => {:success => false}
  def create
    if @current_user
      identity = @current_user.build_identity(identity_params)
      if identity.save
        render :status => 200,
               :json => { :success => true,
                          :info => "Fb authenticated token saved successfully",
                          :data => { :identity => identity.as_json } }
      else
        render :status => 400,
               :json => { :success => false,
                          :info => identity.errors.full_messages.join(", "),:data => {} }
      end
    else
      render :status => 404,
             :json => { :success => false,
                        :info => "User Not Found",
                        :data => {} }
    end
  end

  private
    def identity_params
      params.require(:identity).permit(:fb_id, :access_token)
    end
end
