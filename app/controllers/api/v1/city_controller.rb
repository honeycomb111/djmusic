class Api::V1::CityController <  ApplicationController
  before_action :set_city, only: [:show, :update, :destroy]

  api :GET, '/city', "Fetch all cities"
    description 'City Info'
    example <<-EOS
{
    "success": true,
    "info": "All City Information",
    "data": {
        "cities": [
            {
                "id": 1,
                "name": "Los angles",
                "created_at": "2017-10-26T17:24:53.023Z",
                "updated_at": "2017-10-26T17:24:53.023Z",
                "djs": 0,
                "events": 0
            }
        ]
    }
}
    EOS
  def index
  	@cities = City.all
    city_json = []

    @cities.each_with_index do |city,index|
      events = Event.where(:city_id => index + 1)
      dj_ids = []
      future_events = []
      events.each do |event|
        if event.endTime.present? 
          if event.endTime > DateTime.now
            future_events << event
            dj_ids << event.dj_user_id
          end
        else
          future_events << event
          dj_ids << event.dj_user_id
        end
     
      end
      dj_numbers  = dj_ids.uniq.compact.size
      city_json << city.as_json.merge({:djs => dj_numbers, :events => future_events.size} )  
    end
    
  	render :status => 200,
             :json => { :success => true,
                        :info => "All City Information",
                        :data => {:cities => city_json } }
  end


  def show
  	if @city.blank?
      render :status => 200,
           :json => { :success => true,
                      :info => "City Not Found",
                      :data => {} }
    else
      render :status => 200,
           :json => { :success => true,
                      :info => "City Information",
                      :data => { :city => @city.first.as_json } }
    end
  end

  def create
  	@city = City.new(city_params)

    if @city.save
      render :status => 200,
               :json => { :success => true,
                          :info => "#{@city.class} Information created Successfully",
                          :data => {:city => @city },:status => 200, }
    else
      render :status => 400,:json => { :success => false,:info => @city.errors.full_messages.join(", "),:data => {} }
    end
  end

  def update
  	if @city.blank?
      render :status => 400,
           :json => { :success => false,
                      :info => "City Not Found",
                      :data => {} }
    else
      @city = @city.first
      if @city.update(city_params)
        render :status => 200,
               :json => { :success => true,
                          :info => "#{@city.class} Information Updated Successfully",
                          :data => {:city => @city },:status => 200, }
      else
        render :status => 400,:json => { :success => false,:info => @city.errors.full_messages.join(", "),:data => {} }
      end
    end
  end

  def destroy
  	if @city.blank?
      render :status => 400,
           :json => { :success => false,
                      :info => "City Not Found",
                      :data => {  } }
    else
      @city = @city.first
      @city.destroy
      render :status => 200,
             :json => { :success => true,
                        :info => 'City was successfully destroyed.',
                        :data => {  } }
    end
  end

  private
  	def city_params
  		params.require(:city).permit(:name)
  	end

  	def set_city
      @city = City.where(id: params[:id])
    end

end
