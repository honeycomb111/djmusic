class Api::V1::CoverPhotosController < Api::V1::BaseController
  # before_action :verify_token
  before_action :set_cover_photo, only: [:show, :update, :destroy]
  before_action :verify_token, only: [:create,:update,:destroy, :show]
  
  def_param_group :cover_photo do
    param :id, Integer, "CoverPhoto id ( for Update api)", required: true
    param :authentication_token, String, "Authentication Token", required: true
    param :cover_photo, Hash do
      param :xOffset, Integer, "xOffset"
      param :yOffset, Integer, "yOffset"
      param :Float, Integer, "Float"
      param :source, String, "Source of cover photo"
    end
  end




  # api :GET, '/cover_photos', "Fetch all cover_photos"
  # param :authentication_token, String, "Authentication Token", required: true
  # def index
  # 	@cover_photos = CoverPhoto.all
  # 	render :status => 200,
  #           :json => { :success => true,
  #                       :info => "All CoverPhoto Information",
  #                       :data => {:cover_photos => @cover_photos.as_json(request.base_url) },:status => 200, }
  # end

  api :GET, '/cover_photos/:id', "Show a cover photo"
  param :id, Integer, "cover_photo id", required: true
  param :authentication_token, String, "Authentication Token", required: true
  def show
  	if @cover_photo.blank?
      render :status => 400,
           :json => { :success => false,
                      :info => "CoverPhoto Not Found",
                      :data => {  } }
    else
      render :status => 200,
           :json => { :success => true,
                      :info => "CoverPhoto Information",
                      :data => { :cover_photo => @cover_photo.first.as_json(request.base_url) } }
    end
  end

  # api :POST, '/cover_photos', "Create a cover photo"
  # param_group :cover_photo
  # def create
    
  # 	@cover_photo = CoverPhoto.new

  #   if cover_photo_params[:source]
  #     image_file                   = Paperclip.io_adapters.for(cover_photo_params[:source])
  #     image_file.original_filename = "photo"
  #     image_file.content_type      = "image/jpeg"
  #     @cover_photo.source          = image_file
  #   end  	

  #   if @cover_photo.save
  #     render :status => 200,
  #             :json => { :success => true,
  #                         :info => "#{@cover_photo.class} Information created Successfully",
  #                         :data => {:cover_photo => @cover_photo.as_json(request.base_url) },:status => 200, }
  #   else
  #     render :status => 400,:json => { :success => false,:info => @cover_photo.errors.full_messages.join(", "),:data => {} }
  #   end
  # end

  api :PATCH, '/cover_photos/:id', "Update a cover photo"
  param_group :cover_photo
  def update
  	if @cover_photo.blank?
      render :status => 400,
           :json => { :success => false,
                      :info => "CoverPhoto Not Found",
                      :data => {  } }
    else
      @cover_photo = @cover_photo.first
      if @cover_photo.update(cover_photo_params)
        render :status => 200,
               :json => { :success => true,
                          :info => "#{@cover_photo.class} Information Updated Successfully",
                          :data => {:cover_photo => @cover_photo.as_json(request.base_url) },:status => 200, }
      else
        render :status => 400,:json => { :success => false,:info => @cover_photo.errors.full_messages.join(", "),:data => {} }
      end
    end
  end

  def destroy
  	if @cover_photo.blank?
      render :status => 400,
           :json => { :success => false,
                      :info => "CoverPhoto Not Found",
                      :data => {  } }
    else
      @cover_photo = @cover_photo.first
      @cover_photo.destroy
      render :status => 200,
             :json => { :success => true,
                        :info => 'CoverPhoto was successfully destroyed.',
                        :data => {  } }
    end
  end

  private
  	def cover_photo_params
  		params.permit(:xOffset, :yOffset, :Float, :source)
  	end

  	def set_cover_photo
      @cover_photo = CoverPhoto.where(id: params[:id])
    end

end
