class Api::V1::FollowersController < Api::V1::BaseController
 
  before_action :verify_token
  before_action :current_user
  before_action :find_dj_user, only: [:follow, :unfollow, :all_followers]

  after_action only: [:follow] do
    notification_to_dj_user("#{current_user.name} follow you.", params[:dj_user_id])
  end 
  api :POST, '/followers/', "User follow"
  param :authentication_token, String, "Authentication Token", required: true
  param :dj_user_id, Integer, "Dj user id", required: true  
  param :status, Integer, "Follow(1), Unfollow(0)", required: true   
    error :code => 404, :desc => "DjUser Not Found", :meta => {:success => false}
    error :code => 400, :desc => "Inavlid Params", :meta => {:success => false}
  def follow
	if @dj_user.blank?
		render :status => 404,
       :json => { :success => false,
                  :info => "DjUser Not Found",
                  :data => {} }
	else
		@dj_user = @dj_user.first
# 		 binding.pry
		case params[:status] 
		   
			when 1 
				if @current_user.follows?(@dj_user)
					render :status => 200,
		           :json => { :success => true,
		                      :info => "You already followed to #{@dj_user.name}",
		                      :data => nil }
				else
					@current_user.follow!(@dj_user)
				# 	update_likes(@dj_user)
					render :status => 200,
		           :json => { :success => true,
		                      :info => "Successfully followed to #{@dj_user.name}",
		                      :data => nil }
				end
			when 0
				if  @current_user.follows?(@dj_user) == false
					render :status => 200,
		           :json => { :success => true,
		                      :info => "You already un-followed to #{@dj_user.name}",
		                      :data => nil }
				else
					@current_user.unfollow!(@dj_user)
					render :status => 200,
		           :json => { :success => true,
		                      :info => "successfully un-followed to #{@dj_user.name}",
		                      :data => nil }
				end				
			else
			 	render :status => 400,
		          :json => { :success => false,
		                      :info => "Inavlid Params",
		                      :data => nil }
		    end
		 end
	end			
	  
 # end
 # api :POST, '/followers/follow/:dj_user_id', "User follow"
 # param :authentication_token, String, "Authentication Token", required: true
 # def follow
	# if @dj_user.blank?
	# 	render :status => 400,
 #      :json => { :success => false,
 #                 :info => "Inavlid DjUser Id",
 #                 :data => nil }
	# else
	# 	@dj_user = @dj_user.first
	# 	if @current_user.follows?(@dj_user)
	# 		render :status => 200,
 #          :json => { :success => true,
 #                     :info => "You already followed to #{@dj_user.name}",
 #                     :data => nil }
	# 	else
	# 		@current_user.follow!(@dj_user)
	# 		update_likes(@dj_user)
	# 		render :status => 200,
 #          :json => { :success => true,
 #                     :info => "Successfully followed to #{@dj_user.name}",
 #                     :data => nil }
	# 	end
	# end			
	  
 # end

 # api :POST, '/followers/unfollow/:dj_user_id', "Unfollow user"
 # param :dj_user_id, Integer, "dj_user id"
 # param :authentication_token, String, "Authentication Token", required: true
 # def unfollow
 # 	if @current_user.blank?
	# 		render :status => 400,
 #          :json => { :success => false,
 #                     :info => "Inavlid authentication token",
 #                     :data => {  } }
	# 	else
	# 		if @dj_user.blank?
	# 			render :status => 400,
	#           :json => { :success => false,
	#                       :info => "Inavlid DjUser Id",
	#                       :data => {  } }
	# 		else
	# 			@dj_user = @dj_user.first
	# 			@current_user.unfollow!(@dj_user)
	# 			render :status => 200,
	#           :json => { :success => true,
	#                       :info => "Successfully un-followed to #{@dj_user.name}",
	#                       :data => {  } }
	# 		end			
	# 	end
 # end

  # get all followees of loggedin user

  api :GET, '/followers/all_followees', "List of all followees"
  param :authentication_token, String, "Authentication Token", required: true
  def all_followees
#   	if @current_user.blank?
# 			render :status => 400,
#           :json => { :success => false,
#                       :info => "Inavlid authentication token",
#                       :data => {  } }
# 		else
# 			render :status => 200,
	        render   :json => { :success => true,
	                      :info => "All followees lists of logged-in user",
	                      :data => { :followees => @current_user.followees(DjUser).flat_map(&:as_brief_json), :followees_count => @current_user.followees_count } }
# 		end
  end

  # get all followers of a dj user
  api :GET, '/followers/all_followers/:dj_user_id', "Get all followers of dj user"
  param :dj_user_id, Integer, "dj_user id"
  param :authentication_token, String, "Authentication Token", required: true
   description 'All followers of dj'
    example <<-EOS
{
      "success": true,
    "info": "All followers lists of Ketty.S",
    "data": {
        "followers": [
            {
                "id": 1,
                "name": "User1",
                "title": "123",
                "about": "all about me",
                "facebookURLString": "",
                "cover_photo": {
                    "id": 2,
                    "xOffset": 0,
                    "yOffset": 0,
                    "Float": 0,
                    "source": {
                        "original": "http://localhost:3000/coverphotos/2/73400_original.png?1509037627",
                        "medium": "http://localhost:3000/coverphotos/2/73400_medium.png?1509037627",
                        "thumb": "http://localhost:3000/coverphotos/2/73400_thumb.png?1509037627"
                    }
                },
                "followees_count": 2,
                "djUser": 4,
                "created_at": "2017-10-26T17:28:00.554Z"
            }
        ],
        "followers_count": 1
    }
}
    EOS
    error :code => 404, :desc => "DjUser Not Found", :meta => {:success => false}
  def all_followers
	if @dj_user.blank?
		render :status => 404,
       :json => { :success => false,
                  :info => "DjUser Not Found",
                  :data => {  } }
	else
		@dj_user = @dj_user.first
		render :status => 200,
       :json => { :success => true,
                  :info => "All followers lists of #{@dj_user.name}",
                  :data => { :followers => @dj_user.followers(User).flat_map(&:as_brief_json), :followers_count => @dj_user.followers_count } }
	end			
		
  end

  # to get followees of a user
  # api :GET, '/followers/followees/:user_id', "Get followees of a user"
  api :GET, '/followers/followees/:user_id', "Get followees of a user"
  param :user_id, Integer, "User id", required: true
  param :authentication_token, String, "Authentication Token", required: true
     error :code => 404, :desc => "User Not Found", :meta => {:success => false}
  def followees

	@user = User.where(:id => params[:user_id])
	if @user.blank?
		render :status => 404,
       :json => { :success => false,
                  :info => "User Not Found",
                  :data => {} }
	else
		@user = @user.first
		render :status => 200,
       :json => { :success => true,
                  :info => "All followers lists of #{@user.name}",
                  :data => { :followers =>  @user.followees(DjUser).flat_map(&:as_brief_json) } }
                  #:data => { :followers => @user.followees(DjUser).as_brief_json } }
                 
	end			
		
  end

  private
  	def find_dj_user
  		@dj_user = DjUser.where(:id => params[:dj_user_id])
  	end
	
	def update_likes
		
	end
end
