class Api::V1::SongsController < Api::V1::BaseController
  before_action :verify_token

  def_param_group :song do
    # param :id, Integer, "Event id ( for Update api)", :required => true
    param :authentication_token, String, "Authentication Token", :required => true
    param :song, Hash do
      param :name, String, "Song Name"
      param :artist_name, String, "Artist Name"
      param :event_id, Integer, "Event id"
      param :dj_user_id, Integer, "Dj user id"
      param :user_id, Integer, "User id"
    end
  end


  api :POST, '/songs', "Request a song"
  param_group :song
  error :code => 400, :desc => "Error messages..", :meta => {:success => false}
  def create
    @song = Song.new(song_params)

    if @song.save
      render :status => 200,
               :json => { :success => true,
                          :info => "Request a song Successfully",
                          :data => {song_name: @song.name, artist_name: @song.artist_name }}
    else
      render :status => 400,:json => { :success => false,:info => @song.errors.full_messages.join(", "),:data => {} }
    end
  end

  api :GET, '/songs/request_songs/:event_id/:dj_user_id', "Fetch all requested songs"
  param :event_id, Integer, "event_id", :required => true
  param :dj_user_id, Integer, "dj_user_id", :required => true
  param :authentication_token, String, "Authentication Token", required: true
     description ' All Request Songs response'
    example <<-EOS
{

    "success": true,
    "info": "All Request Songs",
    "data": {
        "songs": [
            {
                "id": 1,
                "name": "My Love",
                "artist_name": "Chris L",
                "event_id": 1,
                "dj_user_id": 1,
                "user_id": 1,
                "status": false,
                "created_at": "2017-11-01T17:47:23.008Z",
                "updated_at": "2017-11-01T17:47:23.008Z"
            }
        ]
    }

 }
    EOS
  def request_songs
    @songs = Song.where("event_id = ? and  dj_user_id = ? ",params[:event_id],params[:dj_user_id]).order('created_at DESC').limit(20)
    # if @songs.blank?
    #     render :status => 404,
    #         :json => { :success => false,
    #                     :info => "Requested Songs Not Found ",
    #                     :data => {  } }
    # else
            render :status => 200,
               :json => { :success => true,
                          :info => "All Request Songs",
                          :data => {:songs => @songs.as_json }
               }
    # end 
  end


  api :POST, '/songs/change_status', "Change status of request song Accept / Decline"
  param :song_id, Integer, "song_id", :required => true
  param :status, String, "status (true / false)", :required => true
  param :authentication_token, String, "Authentication Token", required: true
  error :code => 404, :desc => "Requested Song Not Found ", :meta => {:success => false}
  error :code => 403, :desc => "Permission Error", :meta => {:success => false}
  def change_status
    @current_user = User.find_by(authentication_token: params[:authentication_token])
    @song = Song.where(:id => params[:song_id])
    if @song.blank?
        render :status => 404,
             :json => { :success => false,
                        :info => "Requested Song Not Found ",
                        :data => {  } }
    else
      @song = @song.first
      @song.update_attributes(status: params[:status])
      if @song.dj_user_id ==  @current_user.dj_user_id
        render :status => 200,
                 :json => { :success => true,
                            :info => "Request a song Successfully",
                            :data => {song_name: @song.name, artist_name: @song.status }}
      else
          render :status => 403,
             :json => { :success => false,
                        :info => "Permission Error",
                        :data => {} }
      end
    end 
  end

  private
  # Use callbacks to share common setup or constraints between actions.
    # def set_song
    #   @song = Song.where(id: params[:id])
    # end

    def song_params
      params.require(:song).permit(:name, :artist_name, :event_id, :dj_user_id, :user_id, :status)
    end
end