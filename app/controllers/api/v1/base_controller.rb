class Api::V1::BaseController < ApplicationController
  # skip_before_action :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }
	respond_to :json
	before_action :verify_token
  require 'houston'


  def verify_token
    user = User.find_by(authentication_token: params[:authentication_token])
    if user.nil?
      render :status => 401,
           :json => { :success => false,
                      :info => "Authentication Token Not Present... Or Invalid!" }
    end
  end

  def current_user
    if params[:authentication_token]
      @current_user = User.find_by_authentication_token params[:authentication_token]
    end
  end

   # for sending Apple Push Notifications
    def notification(message, dj_user_id, custom_data)
    
      begin
        if message == "New event arrange in your city"
          Activity.create(activity_name: message, user_id: dj_user_id)

          users = User.where("user_id = ?", dj_user_id)
          token = user.devise_token
          if token.present?
            apn = Houston::Client.development
            if Rails.env.development?
              apn = Houston::Client.development
              apn.certificate = File.read("app/certificates/Certificates_Dev.pem")
            elsif Rails.env.production?
              apn = Houston::Client.production  ## ALL you have to change this to production
              apn.certificate = File.read("app/certificates/Certificates_Production.pem")
            end

            # # An example of the token sent back when a device registers for notifications
            # token = '<ce8be627 2e43e855 16033e24 b4c28922 0eeda487 9c477160 b2545e95 b68b5969>'

            # # Create a notification that alerts a message to the user, plays a sound, and sets the badge on the app
            notification = Houston::Notification.new(device: token)
            notification.alert = message

            # Notifications can also change the badge count, have a custom sound, have a category identifier, indicate available Newsstand content, or pass along arbitrary data.
            notification.badge = 57
            notification.sound = 'sosumi.aiff'
            notification.content_available = true
            notification.mutable_content = true
            notification.custom_data = custom_data

            # And... sent! That's all it takes.
            apn.push(notification)
          end
        else
          #send push to fans
          djUser = DjUser.find_by(id: dj_user_id)
          users = djUser.followers(User)
          users.each do |user|
            Activity.create!(activity_name: message, user_id: user.id)
            token = user.devise_token
            # token = User.find(params[:chats][:reciever_id]).authentication_token
            if token.present?
              apn = Houston::Client.development
              if Rails.env.development?
                apn = Houston::Client.development
                apn.certificate = File.read("app/certificates/Certificates_Dev.pem")
              elsif Rails.env.production?
                apn = Houston::Client.production  ## ALL you have to change this to production
                apn.certificate = File.read("app/certificates/Certificates_Production.pem")
              end

              # # An example of the token sent back when a device registers for notifications
              # token = '<ce8be627 2e43e855 16033e24 b4c28922 0eeda487 9c477160 b2545e95 b68b5969>'

              # # Create a notification that alerts a message to the user, plays a sound, and sets the badge on the app
              notification = Houston::Notification.new(device: token)
              notification.alert = message

              # Notifications can also change the badge count, have a custom sound, have a category identifier, indicate available Newsstand content, or pass along arbitrary data.
              notification.badge = 57
              notification.sound = 'sosumi.aiff'
              notification.content_available = true
              notification.mutable_content = true
              # notification.custom_data = { foo: 'bar' }

              # And... sent! That's all it takes.
              apn.push(notification)
            end
          end
        end

      rescue StandardError => e
        puts e.inspect
      end
    end

    def notification_to_dj_user(message, dj_user_id)
      begin
          Activity.create(activity_name: message, dj_user_id: dj_user_id)
          user = DjUser.find(dj_user_id)
          token = user.devise_token
          # token = User.find(params[:chats][:reciever_id]).authentication_token
          if token.present?
            apn = Houston::Client.development
            if Rails.env.development?
              apn = Houston::Client.development
              apn.certificate = File.read("app/certificates/Certificates_Dev.pem")
            elsif Rails.env.production?
              apn = Houston::Client.production  ## ALL you have to change this to production
              apn.certificate = File.read("app/certificates/Certificates_Production.pem")
            end

            # # An example of the token sent back when a device registers for notifications
            # token = '<ce8be627 2e43e855 16033e24 b4c28922 0eeda487 9c477160 b2545e95 b68b5969>'

            # # Create a notification that alerts a message to the user, plays a sound, and sets the badge on the app
            notification = Houston::Notification.new(device: token)
            notification.alert = message

            # Notifications can also change the badge count, have a custom sound, have a category identifier, indicate available Newsstand content, or pass along arbitrary data.
            notification.badge = 57
            notification.sound = 'sosumi.aiff'
            notification.content_available = true
            notification.mutable_content = true
            # notification.custom_data = { foo: 'bar' }

            # And... sent! That's all it takes.
            apn.push(notification)
          end

      rescue StandardError => e
        puts e.inspect
      end
    end

end
