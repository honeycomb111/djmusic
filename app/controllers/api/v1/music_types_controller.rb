class Api::V1::MusicTypesController < ApplicationController
  before_action :set_music_type, only: [:show, :update, :destroy]

  api :GET, '/music_types', "Fetch all music types"
  def index
  	@music_types = MusicType.all
  	render :status => 200,
             :json => { :success => true,
                        :info => "All MusicType Information",
                        :data => {:music_types => @music_types.as_json },:status => 200, }
  end

  def show
  	if @music_type.blank?
      render :status => 404,
           :json => { :success => false,
                      :info => "MusicType Not Found",
                      :data => {} }
    else
      render :status => 200,
           :json => { :success => true,
                      :info => "MusicType Information",
                      :data => { :music_type => @music_type.first.as_json } }
    end
  end

  def create
  	@music_type = MusicType.new(music_type_params)

    if @music_type.save
      render :status => 200,
               :json => { :success => true,
                          :info => "#{@music_type.class} Information created Successfully",
                          :data => {:music_type => @music_type },:status => 200, }
    else
      render :status => 400,:json => { :success => false,:info => @music_type.errors.full_messages.join(", "),:data => {} }
    end
  end

  def update
  	if @music_type.blank?
      render :status => 400,
           :json => { :success => false,
                      :info => "MusicType Not Found",
                      :data => {  } }
    else
      @music_type = @music_type.first
      if @music_type.update(music_type_params)
        render :status => 200,
               :json => { :success => true,
                          :info => "#{@music_type.class} Information Updated Successfully",
                          :data => {:music_type => @music_type },:status => 200, }
      else
        render :status => 400,:json => { :success => false,:info => @music_type.errors.full_messages.join(", "),:data => {} }
      end
    end
  end

  def destroy
  	if @music_type.blank?
      render :status => 400,
           :json => { :success => false,
                      :info => "MusicType Not Found",
                      :data => {  } }
    else
      @music_type = @music_type.first
      @music_type.destroy
      render :status => 200,
             :json => { :success => true,
                        :info => 'MusicType was successfully destroyed.',
                        :data => {  } }
    end
  end

  private
  	def music_type_params
  		params.require(:music_type).permit(:name, :description, :event_id)
  	end

  	def set_music_type
      @music_type = MusicType.where(id: params[:id])
    end

end
