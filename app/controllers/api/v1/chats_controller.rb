class Api::V1::ChatsController < Api::V1::BaseController
  before_action :verify_token
  before_action :set_chat, only: [:show, :update, :destroy]
  respond_to :json
  require 'houston'
  after_action :notification, only: [:create]

  def_param_group :chats do
    param :id, Integer, "Chat id ( for Update api)", required: true
    param :authentication_token, String, "Authentication Token", required: true
    param :chats, Hash do 
      param :message, String, "Chat Message", required: true
      param :sender_id, Integer, "Chat message sender id", required: true
      param :receiver_id, Integer, "Chat message receiver id", required: true
    end
  end


  # api for create chat message
  # POST /chats
  api :POST, '/chats', 'Create chat'
  param_group :chats
  def create
    @chat = Chat.new(chat_params)

    if @chat.save
      render :status => 200,
               :json => { :success => true,
                          :info => "Chat message created Successfully",
                          :data => {:chat => @chat.as_json }}
    else
      render :status => 400,:json => { :success => false,:info => @chat.errors.full_messages.join(", "),:data => {} }
    end
  end


  # api for update chat message
  # PATCH/PUT /chats/1
  api :PATCH, '/chats/:id', "Update chat messages"
  param_group :chats
  def update
    if @chat.blank?
      render :status => 400,
           :json => { :success => false,
                      :info => "Chat Not Found",
                      :data => {  } }
    else
      @chat = @chat.first
      if @chat.update(chat_params)
        render :status => 200,
               :json => { :success => true,
                          :info => 'Chat message was successfully updated.',
                          :data => {:chat => @chat.as_json }}
      else
        render :status => 400,:json => { :success => false,:info => @chat.errors.full_messages.join(", "),:data => {} }
      end
    end
  end


  # api for delete chat message
  # DELETE /chats/1
  api :DELETE, '/chats/:id', "Delete chat message"
  param :id, Integer, "Chat id", :required => true
  param :authentication_token, String, "Authentication Token", required: true
  def destroy
    if @chat.blank?
      render :status => 400,
           :json => { :success => false,
                      :info => "Chat Not Found",
                      :data => {  } }
    else
      @chat = @chat.first
      @chat.destroy
      render :status => 200,
             :json => { :success => true,
                        :info => 'Chat successfully destroyed.',
                        :data => {  } }
    end
  end


  # api for reas chat messages
  # GET /chats/1
  api :GET, '/chats/:receiver_id', "Fetch all chat messages between two users"
  param :receiver_id, Integer, "receiver_id", :required => true
  param :authentication_token, String, "Authentication Token", required: true
  def show
    @chat = Chat.where("(sender_id = ? AND receiver_id =?) OR (sender_id = ? AND receiver_id =?)", current_user.id,params[:id].to_i, params[:id].to_i, current_user.id)
    # if @chat.blank?
    #   render :status => 400,
    #       :json => { :success => false,
    #                   :info => "Chat Not Found",
    #                   :data => {  } }
    # else
      render :status => 200,
             :json => { :success => true,
                        :info => 'Chat List',
                        :data => {:chat => @chat.as_json }}
    # end
  end

  # api :GET, '/chats/list', "Fetch all chat list"
  # param :authentication_token, String, "Authentication Token", required: true
  # def list
  #   @chat = Chat.where("sender_id = ? Or receiver_id =?", current_user.id, current_user.id)
  #   if @chat.blank?
  #     render :status => 400,
  #         :json => { :success => false,
  #                     :info => "Chat Not Found",
  #                     :data => {  } }
  #   else
  #     render :status => 200,
  #           :json => { :success => true,
  #                       :info => 'Chat List',
  #                       :data => {:chat => @chat.as_json }}
  #   end
  # end

    
  
  private

    # for sending Apple Push Notifications
    def notification
      begin
        token = User.find(params[:chats][:receiver_id]).devise_token
        # token = User.find(params[:chats][:receiver_id]).authentication_token
        if token.present?
          apn = Houston::Client.development
          if Rails.env.development?
            apn = Houston::Client.development
            apn.certificate = File.read("app/certificates/Certificates_Dev.pem")
          elsif Rails.env.production?
            apn = Houston::Client.production  ## ALL you have to change this to production
            apn.certificate = File.read("app/certificates/Certificates_Production.pem")
          end

          # # An example of the token sent back when a device registers for notifications
          # token = '<ce8be627 2e43e855 16033e24 b4c28922 0eeda487 9c477160 b2545e95 b68b5969>'

          # # Create a notification that alerts a message to the user, plays a sound, and sets the badge on the app
          notification = Houston::Notification.new(device: token)
          notification.alert = params[:chats][:message]

          # Notifications can also change the badge count, have a custom sound, have a category identifier, indicate available Newsstand content, or pass along arbitrary data.
          notification.badge = 57
          notification.sound = 'sosumi.aiff'
          notification.content_available = true
          notification.mutable_content = true
          # notification.custom_data = { foo: 'bar' } 

          # And... sent! That's all it takes.
          apn.push(notification)
        end

      rescue StandardError => e
        puts e.inspect
      end
    end

    def chat_params
      params.require(:chats).permit(:message, :sender_id, :receiver_id)
    end

    def set_chat
      @chat = Chat.where(id: params[:id])
    end
end