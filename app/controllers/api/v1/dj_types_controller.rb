class Api::V1::DjTypesController < ApplicationController
  before_action :set_dj_type, only: [:show, :update, :destroy]

  api :GET, '/dj_types', "Fetch all dj types"
  def index
  	@dj_types = DjType.all
  	render :status => 200,
             :json => { :success => true,
                        :info => "All DjType Information",
                        :data => {:dj_types => @dj_types.as_json },:status => 200, }
  end

  def show
  	if @dj_type.blank?
      render :status => 400,
           :json => { :success => false,
                      :info => "DjType Not Found",
                      :data => {  } }
    else
      render :status => 200,
           :json => { :success => true,
                      :info => "DjType Information",
                      :data => { :dj_type => @dj_type.first.as_json } }
    end
  end

  def create
  	@dj_type = DjType.new(dj_type_params)

    if @dj_type.save
      render :status => 200,
               :json => { :success => true,
                          :info => "#{@dj_type.class} Information created Successfully",
                          :data => {:dj_type => @dj_type },:status => 200, }
    else
      render :status => 400,:json => { :success => false,:info => @dj_type.errors.full_messages.join(", "),:data => {} }
    end
  end

  def update
  	if @dj_type.blank?
      render :status => 400,
           :json => { :success => false,
                      :info => "DjType Not Found",
                      :data => {  } }
    else
      @dj_type = @dj_type.first
      if @dj_type.update(dj_type_params)
        render :status => 200,
               :json => { :success => true,
                          :info => "#{@dj_type.class} Information Updated Successfully",
                          :data => {:dj_type => @dj_type },:status => 200, }
      else
        render :status => 400,:json => { :success => false,:info => @dj_type.errors.full_messages.join(", "),:data => {} }
      end
    end
  end

  def destroy
  	if @dj_type.blank?
      render :status => 400,
           :json => { :success => false,
                      :info => "DjType Not Found",
                      :data => {  } }
    else
      @dj_type = @dj_type.first
      @dj_type.destroy
      render :status => 200,
             :json => { :success => true,
                        :info => 'DjType was successfully destroyed.',
                        :data => {  } }
    end
  end

  private
  	def dj_type_params
  		params.require(:dj_type).permit(:name, :description)
  	end

  	def set_dj_type
      @dj_type = DjType.where(id: params[:id])
    end

end
