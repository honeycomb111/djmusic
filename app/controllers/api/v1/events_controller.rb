class Api::V1::EventsController < Api::V1::BaseController
  before_action :verify_token
  before_action :current_user, only: [ :fb_events, :favorite, :unfavorite, :get_favorite]
  before_action :set_event, only: [:show, :edit, :update, :destroy, :favorite, :unfavorite]

  
  after_action only: [:favorite] do
    dj_user_id = Event.find(params[:id]).dj_user_id
    notification_to_dj_user("#{current_user.name} likes your event.", dj_user_id)
  end 

  def_param_group :event do
    param :id, Integer, "Event id ( for Update api)", :required => true
    param :authentication_token, String, "Authentication Token", :required => true
    param :event, Hash do
      param :title, String, "Title"
      param :longDescription, String, "Event Long Description"
      param :shortDescription, String, "Event Short Description"
      param :hostedBy, String, "Event Hosted by"
      param :latitude, Float, "Event place latitude"
      param :longitude, Float, "Event place longtitude"
      param :restrictAge, Integer, "Restricted age for event, only 0,1,2(Age 18, Age 21, All)"
      param :openingTime, Time, "Event Opening Time"
      param :endTime, Time, "Event Closing Time"
      param :attendingCount, Integer, "Event Attending Count"
      param :interestedCount, Integer, "Event Interested Count"
      param :isCanceled, Integer, "Event isCanceled status"
      param :isFree, Integer, "Event isFree status"
      param :fbLink, String, "User facebook link " 
      param :city_id, Integer, "Event City id"
      param :dj_type_id, Integer, "Dj type id", :required => true
      param :musicType, Array, "Event Music Types by Array"
      param :vibeType, Array, "Event Vibe Type by Array"
    end
  end



  # GET /events
  api :GET, '/events', "Fetch all events"
  param :authentication_token, String, "Authentication Token", required: true
  param :dj_user_id, Integer, "Filter by Particular Dj id"
  param :city_id, Integer, "Filter by Particular City id"
  param :music_type_id, Array, "Filter by Music id"
  param :vibe_type_id, Array, "Filter by Vibe id"  
  def index

    if params[:dj_user_id].present?
      @events = Event.where(:dj_user_id => params[:dj_user_id])
    else 
      @events = Event.all
    end
    
    if params[:city_id].present?
      @events = @events.where(:city_id => params[:city_id])
    end
 
    if params[:music_type_id].present? 
         
      @events = @events.select{|event| (event.event_music_relations.pluck(:music_type_id) & params[:music_type_id].map {|x| x.to_i}).size > 0}
    end
    if params[:vibe_type_id].present? 
       @events = @events.select{|event| (event.event_vibe_relations.pluck(:vibe_type_id) & params[:vibe_type_id].map {|x| x.to_i}).size > 0}
    end
   
    render :status => 200,
             :json => { :success => true,
                        :info => "Event Information",
                        :data => {:events => @events.as_json }}
  end

  # GET /events/1
  api :GET, '/events/:id', "Show the event"
  param :id, Integer, "Event id", required: true
  param :authentication_token, String, "Authentication Token", required: true
   error :code => 404, :desc => "Event Not Found", :meta => {:success => false}
  def show
    if @event.blank?
      render :status => 404,
           :json => { :success => false,
                      :info => "Event Not Found",
                      :data => nil }
    else
      render :status => 200,
           :json => { :success => true,
                      :info => "Event Information",
                      :data => { :event => @event.first.as_json } }
    end
  end

  # POST /events
  api :POST, '/events', "Create a event"
  param_group :event
  
  param :xOffset, Integer, "xOffset"
  param :yOffset, Integer, "yOffset"
  param :Float, Integer, "Float"
  param :source, String, "Source of cover photo(Base64)"
  def create
     @event = Event.new(event_params)
    if params[:event][:source]
      cover_photo = CoverPhoto.new(cover_photo_params)
      image_file                   = Paperclip.io_adapters.for(params[:event][:source])
      image_file.original_filename = "photo"
      # image_file.content_type      = "image/jpeg"
      cover_photo.source          = image_file
      @event.cover_photo = cover_photo
    end     

    if @event.save
      update_event_music_relation
      update_event_vibe_relation
      update_gigs
      render :status => 200,
               :json => { :success => true,
                          :info => "#{@event.class} created Successfully",
                          :data => {:event => @event.as_json }}
                          
      users = User.where("city_id = ?", @event.city_id)
      if users.present?
        users.each do |user|
          notification("New event arranged in your city", user.id, @event.as_json )
        end
      end                       
    else
      render :status => 400,:json => { :success => false,:info => @event.errors.full_messages.join(", "),:data => nil}               
    end
  end

  # PATCH/PUT /events/1
  api :PATCH, '/events/:id', "Update the event"
  param_group :event
  error :code => 404, :desc => "Event Not Found", :meta => {:success => false}
  def update
    if @event.blank?
      render :status => 404,
           :json => { :success => false,
                      :info => "Event Not Found",
                      :data => nil }
    else
      @event = @event.first
      # binding.pry
      # if @event.update(event_params) && @event.cover_photo.update(cover_photo_params)
   #   if @event.update(event_params)
        @event.attributes = event_params
        update_event_music_relation
        update_event_vibe_relation
        if params[:event][:source]
          cover_photo = CoverPhoto.new(cover_photo_params)
          image_file                   = Paperclip.io_adapters.for(params[:event][:source])
          image_file.original_filename = "photo"
          # image_file.content_type      = "image/jpeg"
          cover_photo.source          = image_file
          @event.cover_photo = cover_photo
        end  
        if @event.save
          render :status => 200,
                 :json => { :success => true,
                            :info => 'Event was successfully updated.',
                            :data => {:event => @event.as_json } }
        else
          render :status => 400,:json => { :success => false,:info => @event.errors.full_messages.join(", "),:data => {} }
        end
      # end
    end
  end

  # DELETE /events/1
  api :DELETE, 'events/:id', "Delete the event"
  param :id, Integer, "Event id", required: true
  param :authentication_token, String, "Authentication Token", required: true
  error :code => 404, :desc => "Event Not Found", :meta => {:success => false}
  def destroy
    if @event.blank?
      render :status => 404,
           :json => { :success => false,
                      :info => "Event Not Found",
                      :data => {} }
    else
      @event = @event.first
      @event.destroy
      render :status => 200,
             :json => { :success => true,
                        :info => 'Event successfully destroyed.',
                        :data => {} }
    end
  end

  api :GET, '/events/fb_events', "List of fb events"
  param :authentication_token, String, "Authentication Token", required: true
   error :code => 401, :desc => "Fb token Not Present", :meta => {:success => false}
   error :code => 400, :desc => "Params Error", :meta => {:success => false}
  def fb_events
      begin
        if @current_user.identity
          client = Koala::Facebook::API.new @current_user.identity.access_token
          events = client.get_object('me/events')
          render :status => 200,
                 :json => { :success => true,
                            :info => 'Fb events list.',
                            :data => { :fb_events => events } }
        else
          render :status => 401,
                 :json => {:success => false,
                           :info => "Fb token Not Present", :data => {}}
        end
      rescue Exception => ex
        render :status => 400,
               :json => {:success => false,
                         :info => "#{ex.message}", :data => {}}
      end

  end

  api :POST, '/events/:id/favorite', "To make favorite event"
  param :id, Integer, "Event id", required: true
  param :authentication_token, String, "Authentication Token", required: true
    error :code => 404, :desc => "Event Not Found", :meta => {:success => false}
  def favorite 
			if @event.blank?
				render :status => 404,
	           :json => { :success => false,
	                      :info => "Event Not Found",
	                      :data => {} }
			else
				@event = @event.first
				if @current_user.follows?(@event)
					render :status => 200,
		           :json => { :success => true,
		                      :info => "You already favorited the event",
		                      :data => {} }
				else
					@current_user.follow!(@event)
					render :status => 200,
		           :json => { :success => true,
		                      :info => "Successfully favorited to the event",
		                      :data => {} }
				end
			end	
  end
  
  api :DELETE, '/events/:id/favorite', "To make unfavorite event"
  param :id, Integer, "Event id", required: true
  param :authentication_token, String, "Authentication Token", required: true
      error :code => 404, :desc => "Event Not Found", :meta => {:success => false}
  def unfavorite 
			if @event.blank?
				render :status => 400,
	           :json => { :success => false,
	                      :info => "Event Not Found",
	                      :data => {  } }
			else
				@event = @event.first
				if @current_user.follows?(@event)
				  @current_user.unfollow!(@event)
					render :status => 200,
		           :json => { :success => true,
		                      :info => "Successfully unfavorited to the event",
		                      :data => {}}
				else
					render :status => 200,
		           :json => { :success => true,
		                      :info => "You already unfavorited the event",
		                      :data => {} }
				end
			end	
  end

  api :GET, '/events/favorite', "Show all favorite event"
  param :authentication_token, String, "Authentication Token", required: true
  description <<-EOS
    Get all favorite Events of the user 
  EOS
  def get_favorite
     render :status => 200,
     :json => { :success => true,
                :info => 'favorite events list',
                :data => { :favorite => @current_user.followees(Event).as_json}}
   
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.where(id: params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def event_params
      params.require(:event).permit(:title, :longDescription, :shortDescription, :hostedBy, :latitude, :longitude, :restrictAge, :openingTime, :endTime, :attendingCount, :interestedCount, :isCanceled, :isFree, :fbLink, :city_id, :musicType, :vibeType, :dj_type_id, :dj_user_id)
    end

    def cover_photo_params
      params.require(:event).permit(:xOffset, :yOffset, :Float)
    end
    
    def location_params
      params.require(:currentLocation).permit(:lat, :long)
    end
    
    def update_event_music_relation
      unless params[:event][:musicType].nil?
       @event.event_music_relations.clear
        for type in params[:event][:musicType] do
          if @event.event_music_relations.where(:music_type_id => type).size == 0
              @event.event_music_relations.new( music_type_id: type )
          end
        end
      end
    end 
    
    def update_event_vibe_relation
     unless params[:event][:vibeType].nil?
       @event.event_vibe_relations.clear
      for type in params[:event][:vibeType] do
        if @event.event_vibe_relations.where(:vibe_type_id => type).size == 0
            @event.event_vibe_relations.new( vibe_type_id: type )
        end
      end
     end  
    end  
    
    def update_gigs
      if params[:event][:openingTime].nil? == false 
        event_period = Date.parse(params[:event][:openingTime]).strftime("%Y-%d")
        report = Report.where(dj_user_id: params[:event][:dj_user_id]).where(period:event_period).first
        if report.nil? 
          Report.create(dj_user_id: params[:event][:dj_user_id], period: event_period, gigs: 1)
        else
          report.gigs += 1
          report.save
        end
      end
      
    end
end
