class Api::V1::DjUsersController < Api::V1::BaseController
  before_action :verify_token
  before_action :current_user, only: [:create,:update,:destroy]
  before_action :set_dj_user, only: [:show, :update, :destroy, :tip, :received_tips, :post_reviews, :get_reviews]
  # validate :custom_validation_method, :on => :create

  after_action only: [:update] do
    notification("#{@dj_user.name} has updated the profile.", params[:id],  @dj_user.to_json )
  end
  after_action only: [:tip] do
    notification("tipped " +  @dj_user.tip_offs.last.amount.to_s + "$" , params[:id], @dj_user.tip_offs.last.as_json)
    notification_to_dj_user("You have received " + @dj_user.tip_offs.last.amount.to_s +  "$ tips.", params[:id])
  end
  after_action only: [:post_reviews] do
    notification("Review posted on #{@dj_user.name} profile.", params[:id], @dj_user.reviews.last.as_json)
    notification_to_dj_user("You are getting reviews.", params[:id])
  end


  
  def_param_group :dj_user do
    param :id, Integer, "Dj id ( for Update api)", required: true
    param :authentication_token, String, "Authentication Token", required: true
    param :dj_user, Hash do
      param :dj_type_id, Integer, "Dj type id", required: true
      param :title, String, "Titleof DJ"
      param :bio, String, "bio of DJ"
      param :city_id, Integer, "City id"
      param :about, String, "About dj user"
      param :facebookURLString, String, "User facebook string url " 
      param :fbID, String, "User facebook id "
      param :instagramURLString, String, "Dj user instagram url"
      param :twitterURLString, String, "Dj user twitter url"
      # param :isVerified, Boolean, "Dj user isVerified"
      param :email, String, "Email of user"
      param :musicType, Array, "Music Type of Dj(Array)"
      param :vibeType, Array, "Vibe Type of Dj(Array)"
      param :xOffset, Integer, "xOffset"
      param :yOffset, Integer, "yOffset"
      param :Float, Integer, "Float"
      param :source, String, "Source of cover photo"
    end
  end

  # GET /dj_users
  api :GET, '/dj_users', "Fatch all dj users"
  param :authentication_token, String, "Authentication Token", required: true
  param :city_id, Integer, "city id filter"
  param :music_type_id, Array, "music filter by array"
  param :vibe_type_id, Array, "vibe filter by array"
  param :limit, Integer, "limit of dj_users, default 0"
  param :offset,Integer, "start index of dj_users, default 20"
  def index
    dj_offset = params[:offset].nil? ? 0 : params[:offset].to_i
    dj_limit = params[:limit].nil? ? 20 : params[:limit].to_i
  
   @dj_users = params[:city_id].present? ? DjUser.where(:city_id => params[:city_id]) :DjUser.all
   if params[:music_type_id].present? 
     @dj_users = @dj_users.select{|dj_user| (dj_user.dj_music_relations.pluck(:music_type_id) & params[:music_type_id].map {|x| x.to_i}).size > 0}
   end
   if params[:vibe_type_id].present? 
     @dj_users= @dj_users.select{|dj_user| (dj_user.dj_vibe_relations.pluck(:vibe_type_id) & params[:vibe_type_id].map {|x| x.to_i}).size > 0}
   end
    
    render :status => 200,
             :json => { :success => true,
                        :info => "All DjUser Information",
                        :data => {:dj_users => @dj_users.offset(dj_offset).limit(dj_limit).as_json },:status => 200 }
  end

  # GET /dj_users/1
  api :GET, '/dj_users/:id', "Show a dj user"
  param :id, Integer, "dj_user id", :required => true
  param :authentication_token, String, "Authentication Token", required: true
  def show
    # if @dj_user.blank?
    #   render :status => 400,
    #       :json => { :success => false,
    #                   :info => "DjUser Not Found",
    #                   :data => nil }
    # else
      render :status => 200,
           :json => { :success => true,
                      :info => "DjUser Information",
                      :data => { :dj_user => @dj_user.as_json } }
    # end
  end
  
  api :POST, '/dj_users/:id/tip', "User tip"
  param :id, Integer, "dj_user id", required: true
  param :authentication_token, String, "Authentication Token", required: true
  param :number, String, "card number", required: true
  param :exp_month, Integer, "Exp Month", required: true  
  param :exp_year, Integer,  "Exp Year", required: true  
  param :cvc, Integer, "cvc", required: true  
  param :tip, Integer, "tip usd", required: true  
  error :code => 404, :desc => "Activities Not Found", :meta => {:success => false, :info => {:base => "[reason]"}}
  def tip
    if @dj_user.blank?
      render :status => 404,
           :json => { :success => false,
                      :info => "DjUser Not Found",
                      :data => {} }
    else
      @current_user = User.find_by(authentication_token: params[:authentication_token])
      if @dj_user.pay_with_card(params, @current_user)
        render :status => 200,
          :json => { :success => true,
                      :info => "Tip has been successfully transmit.",
                      :data => {}
                    }
      else
        render :status => 400,
           :json => { :success => false,
                      :info => @dj_user.errors,
                      :data => {} }
      end
    end
  end
  
  api :GET, '/dj_users/:id/received_tips', "User received tips"
  param :id, Integer, "dj_user id", :required => true
  param :authentication_token, String, "Authentication Token", required: true
  error :code => 404, :desc => "DjUser Not Found", :meta => {:success => false}
    description 'Sign in with email and password'
    example <<-EOS
{
    "success": true,
    "info": "tip from all users",
    "data": {
        "tips": [
            {
                "amount": "70.0",
                "created_at": "2017-10-30T19:07:55.078Z",
                "user": {
                    "id": 1,
                    "name": "john",
                    "title": "dj fan",
                    "cover_photo": {
                        "id": 2,
                        "xOffset": 0,
                        "yOffset": 0,
                        "Float": 0,
                        "source": {
                            "original": "http://localhost:3000/coverphotos/2/73400_original.png?1509037627",
                            "medium": "http://localhost:3000/coverphotos/2/73400_medium.png?1509037627",
                            "thumb": "http://localhost:3000/coverphotos/2/73400_thumb.png?1509037627"
                        }
                    }
                }
            }
        ],
        ...
        "totalAmount": 70
    }
}
    EOS
  def received_tips
    if @dj_user.blank?
      render :status => 404,
           :json => { :success => false,
                      :info => "DjUser Not Found",
                      :data => {} }
    else
      render :status => 200,
          :json => { :success => true,
                      :info => "tip from all users",
                      :data => {:tips => @dj_user.tip_offs.as_json,
                                :totalAmount => @dj_user.tip_offs.pluck(:amount).map(&:to_f).sum
                                 } 
                    }
    end
  end

  # POST /dj_users
  api :POST, '/dj_users', "Create a dj user"
  param_group :dj_user
  def create
    @dj_user = DjUser.new(dj_user_params)
    if params[:dj_user][:source]
      cover_photo = CoverPhoto.new(cover_photo_params)
      image_file                   = Paperclip.io_adapters.for(params[:dj_user][:source])
      image_file.original_filename = "photo"
      # image_file.content_type      = "image/jpeg"
      cover_photo.source          = image_file
      @dj_user.cover_photo = cover_photo
    end

    @dj_user.user_id = @current_user.id
    update_dj_music_relation
    update_dj_vibe_relation
    if @dj_user.save
        render :status => 201,
             :json => { :success => true,
                        :info => "Successfully Registered",
                        :data => { "#{@dj_user.class.to_s.downcase}"=>@dj_user.as_json} }       
     else
       render :status => 400,:json => { :success => false,:info => @dj_user.errors.full_messages.join(", "),:data => nil}
    end 
    
    
    
    
    
    # cover_photo = CoverPhoto.new(cover_photo_params)
    # if params[:dj_user][:source]
    #   image_file                   = Paperclip.io_adapters.for(params[:dj_user][:source])
    #   image_file.original_filename = "photo"
    #   # image_file.content_type      = "image/jpeg"
    #   cover_photo.source          = image_file
    # end  	
  
    
    # if cover_photo.save
    #   dj_user = cover_photo.build_dj_user(dj_user_params)
    #   cover_photo.dj_user.user_id = @current_user.id
    #   if dj_user.save
    #     @dj_user = cover_photo.dj_user
    #     @dj_user.user = @current_user
  
    #     update_dj_music_relation
    #     update_dj_vibe_relation
    #     @dj_user.save
    #     render :status => 200,
    #             :json => { :success => true,
    #                         :info => "#{@dj_user.class} created Successfully",
    #                         :data => {:dj_user => @dj_user },:status => 200 }
      
    #   else 
    #     render :status => 400,:json => { :success => false,:info => dj_user.errors.full_messages.join(", "),:data => {}}
    #   end
    # else
    #     render :status => 400,
    #       :json => { :success => false,
    #                   :info => "coverphoto need",
    #                   :data => {} }
    # end
  end


  # PATCH/PUT /dj_users/1
  api :PATCH, '/dj_users/:id', "Update dj user"
  param_group :dj_user
  error :code => 403, :desc => "Permission Error", :meta => {:success => false}
  error :code => 400, :desc => "Param issues", :meta => {:success => false}
  error :code => 404, :desc => "DjUser Not Found", :meta => {:success => false}
  def update
    if @dj_user.blank?
      render :status => 404,
           :json => { :success => false,
                      :info => "DjUser Not Found",
                      :data => nil }
    else
      if @dj_user.user.id == @current_user.id
         @dj_user.assign_attributes(dj_user_params) 
         if params[:dj_user][:source]
          cover_photo = CoverPhoto.new(cover_photo_params)
          image_file                   = Paperclip.io_adapters.for(params[:dj_user][:source])
          image_file.original_filename = "photo"
          # image_file.content_type      = "image/jpeg"
          cover_photo.source          = image_file
          @current_user.cover_photo = cover_photo
        end           
        update_dj_music_relation
        update_dj_vibe_relation
        if @dj_user.save
          # @dj_user.cover_photo.update(cover_photo_params)
          render :status => 200,
                :json => { :success => true,
                            :info => "#{@dj_user.class} Information Updated Successfully",
                            :data => {:dj_user => @dj_user }}
        else
          render :status => 400,:json => { :success => false,:info => @dj_user.errors.full_messages.join(", "),:data => {} }
        end
      else
         render :status => 403,
                        :json => { :success => false,
                            :info => "Permission Error",
                            :data => {} }
      end
    end
  end

  # DELETE /dj_users/1
  api :DELETE, '/dj_users/:id', "Delete a dj user"
  param :id, Integer, "dj_user id", :required => true
  param :authentication_token, String, "Authentication Token", required: true
  error :code => 403, :desc => "Permission Error", :meta => {:success => false}
  error :code => 404, :desc => "DjUser Not Found", :meta => {:success => false}
  def destroy
    if @dj_user.blank?
      render :status => 404,
           :json => { :success => false,
                      :info => "DjUser Not Found",
                      :data => {} }
    else
      if @dj_user.user.id == @current_user.id
        @dj_user.destroy
        render :status => 200,
               :json => { :success => true,
                          :info => 'Dj user was successfully destroyed.',
                          :data => {} }
      else
       render :status => 403,
              :json => { :success => false,
                  :info => "Permission Error",
                  :data => {} }
      end
    end
  end

  # POST .
  api :POST, '/dj_users/:id/reviews', "Create a review"
  param :id, Integer, "dj_user_id" , :required => true
  param :authentication_token, String, "Authentication Token", required: true
  error :code => 409, :desc => "Review is already created", :meta => {:success => false}
  def post_reviews
      @current_user = User.find_by(authentication_token: params[:authentication_token])
      case  review = @dj_user.post_review(params, @current_user)
        when 1
          render :status => 409,
            :json => { :success => false,
                        :info => "Review is already created",
                        :data => {}
                    }
        when -1 
          render :status => 400,
             :json => { :success => false,
                        :info => @dj_user.errors,
                        :data => {} }
        else
          render :status => 200,
            :json => { :success => true,
                        :info => "Review successfully created.",
                        :data => {:review => review.as_json}}
      end
      
  end

  # GET /dj_users/1/reviews
  api :GET, '/dj_users/:id/reviews', "Fetch users reviews"
  param :id, Integer, "dj_user_id" , :required => true
  param :authentication_token, String, "Authentication Token", required: true
  error :code => 404, :desc => "DjUser Not Found", :meta => {:success => false}
  def get_reviews
    if @dj_user.blank?
      render :status => 404,
           :json => { :success => false,
                      :info => "DjUser Not Found",
                      :data => {} }
    else
     render :status => 200,
            :json => { :success => true,
                        :info => "reviews for dj",
                        :data => {"reviews":  @dj_user.reviews.as_json, "reviewsCount": @dj_user.reviews.size}
              
            }
              
    end
  end


  # GET /dj_users/1/my_activities
  api :GET, '/dj_users/:dj_user_id/my_activities', 'Fetch all activities'
  param :dj_user_id, Integer, "dj_user_id" , :required => true
  param :authentication_token, String, "Authentication Token", required: true
  def my_activities
    @activities = Activity.where("dj_user_id = ?", params[:dj_user_id])
    # if @activities.blank?
    #     render :status => 404,
    #         :json => { :success => false,
    #                     :info => "Activities Not Found ",
    #                     :data => nil }
    # else
         render :status => 200,
          :json => { :success => true,
                      :info => "Activity List",
                      :data => {:activity => @activities.as_json }}
              
            
    # end 
  end

  # GET /dj_users/1/my_activities
  # api :GET, '/dj_users/chat_users', 'Fetch all recent chat users'
  # param :authentication_token, String, "Authentication Token", required: true
  # def chat_users
  #   chat_users =  Chat.where("(sender_id = ? ) OR (receiver_id =?)", current_user.id, current_user.id).order("created_at ASC")
  #   users = []
  #   # if chat_users.present?
  #     chat_users.each do |chat|
  #       users << chat.sender_id
  #       users << chat.receiver_id
  #     end

  #     @chat = chat_users.last.message
  #     @usrs = users.uniq.delete_if{|i|i==current_user.id}
  #     render :chat_users
  #   # else
  #   #   render :status => 404,
  #   #         :json => { :success => false,
  #   #                     :info => "Chat Not Found ",
  #   #                     :data => nil }
  #   # end
  # end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dj_user
      @dj_user = DjUser.find_by(id: params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def dj_user_params
      params.require(:dj_user).permit(:dj_type_id, :name, :title, :bio, :city_id, :about, :facebookURLString, :fbID, 
        :instagramURLString, :twitterURLString, :musicType, :vibeType, :email, :limit, :offset)
    end
    
    def cover_photo_params
      params.require(:dj_user).permit(:xOffset, :yOffset, :Float)
    end
    
    def update_dj_music_relation
      unless params[:dj_user][:musicType].nil?
       @dj_user.dj_music_relations.clear
        for type in params[:dj_user][:musicType] do
          if @dj_user.dj_music_relations.where(:music_type_id => type).size == 0
              @dj_user.dj_music_relations.new( music_type_id: type )
          end
        end
      end
    end 
    
    def update_dj_vibe_relation
     unless params[:dj_user][:vibeType].nil?
       @dj_user.dj_vibe_relations.clear
      for type in params[:dj_user][:vibeType] do
        if @dj_user.dj_vibe_relations.where(:vibe_type_id => type).size == 0
            @dj_user.dj_vibe_relations.new( vibe_type_id: type )
        end
      end
     end  
    end     
end
