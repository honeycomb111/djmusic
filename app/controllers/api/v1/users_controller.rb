class Api::V1::UsersController < Api::V1::BaseController
	before_action :verify_token, :except => :create
	before_action :current_user, :except => :create

  def_param_group :users do
    param :authentication_token, String, "Authentication Token ( for Update api)", required: true
    param :users, Hash do
      param :name, String, "Name of the user"
      param :email, String, "Email of user"
      param :password, String, "Password of the user"
      param :password_confirmation, String, "Password Confirmation"
      param :title, String, "Title"
      param :birthday, Date, "Birthday of user(YYYY-MM-DD)"
      param :about, String, "About user"
      param :facebookURLString, String, "User facebook url "
      param :soundCloudURL, String, "User soundcloud url "
      param :city_id, Integer, "User City Id"

    end
  end

  api :POST, '/users', "Create a new user"
  param_group :users
  param :xOffset, Integer, "xOffset of userphoto'crop"
  param :yOffset, Integer, "yOffset of userphoto'crop"
  param :Float, Integer, "Float% 1-100 of userphoto"
  param :source, String, "Base64 of cover photo"
  def create
    
    user = User.new(user_params)
    if params[:users][:source]
      cover_photo = CoverPhoto.new(cover_photo_params)
      image_file                   = Paperclip.io_adapters.for(params[:users][:source])
      image_file.original_filename = "photo"
      # image_file.content_type      = "image/jpeg"
      cover_photo.source          = image_file
      user.cover_photo = cover_photo
    end
    if user.save
        render :status => 200,
             :json => { :success => true,
                        :info => "Successfully Registered",
                        :data => { "#{user.class.to_s.downcase}"=>user.as_json,
                                   :authentication_token => user.authentication_token } }       
     else
       render :status => 400,:json => { :success => false,:info => user.errors.full_messages.join(", "),:data => nil}
    end 
  end

  api :GET, '/users/:id', "Fetch details of user"
  param :authentication_token, String, "Authentication Token", required: true
  def show
		  @user = User.find(params[:id])
			render :status => 200,
          :json => { :success => true,
                      :info => "User Detail",
                      :data => { :user => @current_user.as_brief_json } }
		# end
	end

  api :GET, '/users', "Get details of current user"
  param :authentication_token, String, "Authentication Token", required: true
  error :code => 401, :desc => "Authentication Token Not Present... Or Invalid!", :meta => {:success => false}
	def index
			render :status => 200,
           :json => { :success => true,
                      :info => "User Details",
                      :data => { :user => @current_user.as_json } }
		
	end

  api :PATCH, '/users', "Update user profile"
  param_group :users
  error :code => 400, :desc => "Error messages..", :meta => {:success => false}
  error :code => 404, :desc => "User Not Found", :meta => {:success => false}
	def update
	  
   unless @current_user.blank?

    @current_user.assign_attributes(user_params) 
    if params[:users][:source]
      cover_photo = CoverPhoto.new(cover_photo_params)
      image_file                   = Paperclip.io_adapters.for(params[:users][:source])
      image_file.original_filename = "photo"
      # image_file.content_type      = "image/jpeg"
      cover_photo.source          = image_file
       @current_user.cover_photo = cover_photo
    end
    if  @current_user.save
      # if @current_user.update(user_params) && @current_user.cover_photo.update(cover_photo_params)
        render :status => 200,
             :json => { :success => true,
                        :info => "#{@current_user.class} Updated Successfully",
                        :data => {:user => @current_user }}
      else
        render :status => 400,:json => { :success => false,:info => @current_user.errors.full_messages.join(", "),:data => nil }
      end
    else
      render :status => 404,:json => { :success => false,:info => "User Not Found",:data => nil }
    end
  end

  # GET /my_activities
  api :GET, '/users/my_activities', 'Fetch all activities'
  param :authentication_token, String, "Authentication Token", required: true
  def my_activities
    @activities = Activity.where("user_id = ?", @current_user.id)
    # if @activities.blank?
    #     render :status => 404,
    #         :json => { :success => false,
    #                     :info => "Activities Not Found ",
    #                     :data => nil }
    # else
     render :status => 200,
         :json => { :success => true,
                    :info => "Activity List",
                    :data => {:activity => @activities.as_json } }
     
    # end 
  end

  # GET /dj_users/1/my_activities
  api :GET, '/users/chat_users', 'Fetch all recent chat users'
  param :authentication_token, String, "Authentication Token", required: true

  def chat_users
    chats =  Chat.where("(sender_id = ? ) OR (receiver_id =?)", current_user.id, current_user.id).order("created_at ASC")
    users_list = []
    chats_list = {}
      chats.each do |chat|
        if chat.sender_id == current_user.id 
          chats_list[chat.receiver_id] = chat
        else
          chats_list[chat.sender_id] = chat
        end
        users_list << chat.sender_id
        users_list << chat.receiver_id
      end
      users_list = users_list.uniq.delete_if{|i|i==current_user.id}
      result_json = []
      users_list.reverse_each {|user| result_json << chats_list[user] }
        
      
      render :status => 200,
             :json => { :success => true,
                        :info => "Chat List ",
                        :data => {:list => result_json} }

    
  end

  
	private
		def user_params
        params.require(:users).permit(:name, :email, :password, :password_confirmation, 
          :title, :birthday, :about, :facebookURLString, :soundCloudURL, :city_id)
    end

    def cover_photo_params
      params.require(:users).permit(:xOffset, :yOffset, :Float)
    end
  
end
