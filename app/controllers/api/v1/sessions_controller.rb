class Api::V1::SessionsController < Devise::SessionsController
  skip_before_action :verify_signed_out_user, only: :destroy
  respond_to :json
  before_action :ensure_params_exist


  api :POST, '/users/sign_in', "Fetch User Profile"
  def_param_group :users do
    param :users, Hash do 
      param :email, String, "User email", required: true
      param :password, String, "User Password", required: true
      param :token, String, "Devise Token", required: false
    end
  end 
  
  
  description 'Sign in with email and password'
    example <<-EOS
{
    "info": "Login succeed!",
    "data": {
        "user": {
            "id": 1,
            "name": "Trevor.M",
            "title": "DJ Fan",
            "birthday": 1987-06-07,
            "about": "",
            "facebookURLString": "",
            "cover_photo": {
                "id": 2,
                "xOffset": 0,
                "yOffset": 0,
                "Float": 0,
                "source": {
                    "original": "link..",
                    "medium": "link..",
                    "thumb": "link.."
                }
            },
            "email": "trevor@gmail.com",
            "followees_count": 100,
            "djUser": 2,
            "created_at": "2017-10-26T17:28:00.554Z",
            "updated_at": "2017-10-28T17:31:38.266Z"
        },
        "authentication_token": "h9nf9TdYspLP3cfxdkEd"
    },
    "success": true
}
    EOS
  error :code => 401, :desc => "Incorrect username or password", :meta => {:success => false}
 
  param_group :users
  def create
    # Fetch params
    email = params[:users][:email].downcase if params[:users][:email]
    password = params[:users][:password] if params[:users]
    token = params[:users][:token] if params[:users][:token]

    id = User.find_by(email: email).try(:id) if email.presence
    # Validations
    if request.format != :json
      render status: 401, json: { info: 'The request must be JSON.' , success: false}
      return
    end

    if email.nil? or password.nil?
      render status: 401, json: { info: 'The request MUST contain the user email and password.' , success: false}
      return
    end

    # Authentication
    user = User.where(["lower(email) = :value", { :value => email.downcase }]).first

    if user
      if user.valid_password? password
        user.restore_authentication_token!
        user.authentication_token = Devise.friendly_token
        user.devise_token = token
        user.save
        # Note that the data which should be returned depends heavily of the API client needs.
        render status: 200, json: { info: "Login succeed!", :data => { user: user.as_json, :authentication_token => user.authentication_token}, success: true }
      else
        render status: 401, json: { info: 'Invalid email or password.', success: false }
      end
    else
      render status: 401, json: { info: 'Invalid email or password.', success: false }
    end
  end
  
  

  api :DELETE, '/users/sign_out', "Sign Out"
    description 'Sign in with email and password'
    example <<-EOS
{
    "info": "User Successfully Logged Out",
    "success": true
}
  EOS
  param :users, Hash do 
    param :authentication_token, String, "Authentication Token", required: true
  end
  def destroy
    # Fetch params
    user = User.find_by(authentication_token: params[:users][:authentication_token])

    if user.nil?
      render status: 400, json: { info: 'Invalid token.', success: false}
    else
      user.authentication_token = nil
      user.save!
      render :status => 200,
           :json => { :success => true,
                      :info => "User Successfully Logged Out",
                    }
    end
  end

  def failure
    render :status => 400,
           :json => { :success => false,
                      :info => "Login Failed",
                    }
  end

  protected
    def ensure_params_exist
      return unless params[:users].blank?
      render :json=>{:success=>false, :info=>"invalid request"}, :status => 400
    end
end