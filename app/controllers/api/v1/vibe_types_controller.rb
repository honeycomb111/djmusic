class Api::V1::VibeTypesController < ApplicationController
  before_action :set_vibe_type, only: [:show, :update, :destroy]

  api :GET, '/vibe_types', "Fetch all vibe types"
  def index
  	@vibe_types = VibeType.all
  	render :status => 200,
             :json => { :success => true,
                        :info => "All VibeType Information",
                        :data => {:vibe_types => @vibe_types.as_json },:status => 200, }
  end

  def show
  	if @vibe_type.blank?
      render :status => 400,
           :json => { :success => false,
                      :info => "VibeType Not Found",
                      :data => {} }
    else
      render :status => 200,
           :json => { :success => true,
                      :info => "VibeType Information",
                      :data => { :vibe_type => @vibe_type.first.as_json } }
    end
  end

  def create
  	@vibe_type = VibeType.new(vibe_type_params)

    if @vibe_type.save
      render :status => 200,
               :json => { :success => true,
                          :info => "#{@vibe_type.class} Information created Successfully",
                          :data => {:vibe_type => @vibe_type },:status => 200, }
    else
      render :status => 400,:json => { :success => false,:info => @vibe_type.errors.full_messages.join(", "),:data => {} }
    end
  end

  def update
  	if @vibe_type.blank?
      render :status => 400,
           :json => { :success => false,
                      :info => "VibeType Not Found",
                      :data => {  } }
    else
      @vibe_type = @vibe_type.first
      if @vibe_type.update(vibe_type_params)
        render :status => 200,
               :json => { :success => true,
                          :info => "#{@vibe_type.class} Information Updated Successfully",
                          :data => {:vibe_type => @vibe_type },:status => 200, }
      else
        render :status => 400,:json => { :success => false,:info => @vibe_type.errors.full_messages.join(", "),:data => {} }
      end
    end
  end

  def destroy
  	if @vibe_type.blank?
      render :status => 400,
           :json => { :success => false,
                      :info => "VibeType Not Found",
                      :data => {  } }
    else
      @vibe_type = @vibe_type.first
      @vibe_type.destroy
      render :status => 200,
             :json => { :success => true,
                        :info => 'VibeType was successfully destroyed.',
                        :data => {  } }
    end
  end

  private
  	def vibe_type_params
  		params.require(:vibe_type).permit(:name, :description)
  	end

  	def set_vibe_type
      @vibe_type = VibeType.where(id: params[:id])
    end

end
