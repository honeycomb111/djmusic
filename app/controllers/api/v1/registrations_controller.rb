class Api::V1::RegistrationsController < Devise::RegistrationsController
  prepend_before_action :require_no_authentication
  respond_to :json

  # def_param_group :users do
  #     param :name, String, "Name of the user"
  #     param :email, String, "Email of user"
  #     param :password, String, "Password of the user"
  #     param :password_confirmation, String, "Password Confirmation"
  #     param :title, String, "Title"
  #     param :birthday, String, "Birthday of user"
  #     param :about, String, "About user"
  #     param :facebookURLString, String, "User facebook string url "
  # end

  # api :POST, '/users/sign_up', "Sign Up"
  # param_group :users
  # def create
  #   build_resource(user_params) if params[:users]
  #   if resource.save
  #     render :status => 200,
  #         :json => { :success => true,
  #                     :info => "Successfully Registered The #{resource.class}",
  #                     :data => { "#{resource.class.to_s.downcase}"=>resource.as_json,
  #                               :authentication_token => resource.authentication_token } }
  #   else
  #     render :status => 400,
  #           :json => { :success => false,
  #                       :info => resource.errors.full_messages.join(", "),
  #                       :data => {} }
  #   end
  # end

  private
    def user_params
      params.require(:users).permit(:name, :email, :password, :password_confirmation, 
          :title, :birthday, :about, :facebookURLString )
    end
end