class Api::V1::BarsController < Api::V1::BaseController
  include Api::V1::BarsHelper

  require 'yelp'
  require 'rest-client'

  # before_action :set_vibe_type, only: [:show, :update, :destroy]
  api :GET, '/bars/', 'Get Bars Information'
  param :yelp, [true, false], "Request from Yelp", required: true
  param :latitude, Float, "Latitude of the location you want to search nearby.", required: true
  param :longitude, Float, "Longitude of the location you want to search nearby.", required: true
  param :radius, Float, "Search radius in meters. If the value is too large, a AREA_TOO_LARGE error may be returned. The max value is 40000 meters (25 miles).", required: true
  param :city_id, Integer, "city id"
  param :music_type_id, Array, "Filter by Music id"
  param :vibe_type_id, Array, "Filter by Vibe id"  
  param :limit, Integer, "limit of bars, default 0"
  param :offset,Integer, "start index of bars, default 50"
description <<-EOS
   can get bars using city_id or gps location with radius
   city_id has high priority than gps location
   Response {
   
           'success': "true",
           'info': "All Bars Information",
           'data':  {
              'count' : 3000,
              'bars' : [
                "id": 115,
                "rating": 4,
                "price": "$",
                "phone": "+16263540267",
                "is_closed": false,
                "categories_alias": "[\"mexican\", \"divebars\"]",
                "categories_title": "[\"Mexican\", \"Dive Bars\"]",
                "review_count": 299,
                "name": "La Chuperia",
                "url": "https://www.yelp.com/biz/la-chuperia-los-angeles?adjust_creative=ANTuRq696nDPkE9waqjicw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=ANTuRq696nDPkE9waqjicw",
                "lat": 34.06175,
                "long": -118.21331,
                "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/dpkTREr1GG-mWYVTFje1aA/o.jpg",
                "created_at": "2017-11-03T10:09:01.433Z",
                "updated_at": "2017-11-03T10:09:01.433Z",
                "bar_id": "la-chuperia-los-angeles",
                "display_phone": "(626) 354-0267",
                "address1": "1145 N Mission Rd",
                "address2": "",
                "address3": "",
                "city": {
                    "id": 2,
                    "name": "Los Angeles",
                    "created_at": "2017-10-26T20:35:55.811Z",
                    "updated_at": "2017-10-26T20:35:55.811Z"
                },
                "zip_code": "90033",
                "country": "US",
                "state": "CA",
                "display_address": "[\"1145 N Mission Rd\", \"Los Angeles, CA 90033\"]",
                "city_id": 2,
                "hours_type": "REGULAR",
                "is_overnight": true,
                "bar_end": "[\"0145\", \"0145\", \"0145\", \"0145\", \"0145\", \"0145\", \"0145\"]",
                "day": "[0, 1, 2, 3, 4, 5, 6]",
                "start": "[\"1200\", \"1200\", \"1200\", \"1200\", \"1200\", \"1200\", \"1200\"]",
                "distance": 20821.5
            },
            ...
           ]}
        }
  EOS

     BARS_URL = "https://api.yelp.com/v3/businesses/search"
      AUTH_URL = "https://api.yelp.com/oauth2/token"   
    
    # def get_auth 
    #     ret = RestClient.post AUTH_URL, {:client_id => Rails.application.secrets.yelp_id, :client_secret => Rails.application.secrets.yelp_secret}
    #     return JSON.parse(ret)["access_token"]
    # end    

  def index
    bar_offset = params[:offset].nil? ? 0 : params[:offset].to_i
    bar_limit = params[:limit].nil? ? 50 : params[:limit].to_i
    if params[:latitude] != nil and params[:longitude] != nil and params[:radius] != nil 
        @bars = []
        Bar.all.each do |bar|
           dist = bar.get_distance([params[:latitude].to_f, params[:longitude].to_f ]) 
           if dist < params[:radius].to_f
              bar.distance = dist
              if(params[:city_id] == nil ) or (params[:city_id].to_i == bar.city_id)
                @bars << bar
              end
           end
        end
        
        if params[:music_type_id].present? 
          @bars = @bars.select{|bar| (bar.bar_music_relations.pluck(:music_type_id) & params[:music_type_id].map {|x| x.to_i}).size > 0}
        end
        if params[:vibe_type_id].present? 
           @bars = @bars.select{|bar| (bar.bar_vibe_relations.pluck(:vibe_type_id) & params[:vibe_type_id].map {|x| x.to_i}).size > 0}
        end
        
        @bars.sort! { |a,b| a.distance <=>b.distance }
        render :status => 200,
           :json => { :success => true,
                       :info => "All Bars Information",
                       :data => { :bar => @bars.slice(bar_offset, bar_offset + bar_limit).as_json, :count => @bars.size}}
                       
    else
        if params[:city_id] 
         @bars = City.find(params[:city_id]).bars
         
        if params[:music_type_id].present? 
          @bars = @bars.select{|bar| (bar.bar_music_relations.pluck(:music_type_id) & params[:music_type_id].map {|x| x.to_i}).size > 0}
        end
        if params[:vibe_type_id].present? 
           @bars = @bars.select{|bar| (bar.bar_vibe_relations.pluck(:vibe_type_id) & params[:vibe_type_id].map {|x| x.to_i}).size > 0}
        end
         render :status => 200,
               :json => { :success => true,
                           :info => "All Bars Information",
                           :data => { :bar => @bars.slice(bar_offset, bar_offset + bar_limit).as_json, :count => @bars.size }  }
        else
            render :status => 400,
               :json => { :success => false,
                           :info => "Parameter Missing",
                           :data => nil}
        end
    end     
    

  end



  private
end
