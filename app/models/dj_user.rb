# == Schema Information
#
# Table name: dj_users
#
#  id                 :integer          not null, primary key
#  name               :string
#  email              :string
#  title              :string
#  about              :text
#  fbID               :string
#  statsGigs          :integer
#  statsTips          :integer
#  statsRequests      :integer
#  statsLikes         :integer
#  statsReviews       :integer
#  instagramURLString :string
#  twitterURLString   :string
#  facebookURLString  :string
#  isVerified         :boolean
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  user_id            :integer
#  followers_count    :integer          default(0)
#  dj_type_id         :integer
#  cover_photo_id     :integer
#  city_id            :integer
#  bio                :text
#
# Indexes
#
#  index_dj_users_on_city_id     (city_id)
#  index_dj_users_on_dj_type_id  (dj_type_id)
#

class DjUser < ApplicationRecord
  validates_uniqueness_of :email

  belongs_to :user, optional: true
  has_many :events
  belongs_to :dj_type, optional: true
  
  belongs_to :cover_photo, optional: true
  has_many :tip_offs, dependent: :destroy
  has_many :reports , dependent: :destroy
  has_many :reviews,  dependent: :destroy
  belongs_to  :city, optional: true
  has_many :dj_music_relations
  has_many :music_types , through: :dj_music_relations
  
  has_many :dj_vibe_relations 
  has_many :vibe_types , through: :dj_vibe_relations
  has_many :songs
  # has_many :activities, dependent: :destroy
  acts_as_followable
  def as_json(options={})
   
    # (Year( CloseDate ) * 12 + Month(CloseDate)) - (Year(  TODAY() ) * 12 + Month( TODAY() ))
    # DateTime.now.strftime("%Y-%d")
    gigs_report = Array.new(12) {|i| 0 }
    likes_report = Array.new(12) {|i| 0 }
    tips_report = Array.new(12) {|i| 0 }
    current_year  = DateTime.now.strftime("%Y").to_i
    current_month = DateTime.now.strftime("%m").to_i
    self.reports.each do |report|
      report_year = report.period.split("-")[0].to_i
      report_month = report.period.split("-")[1].to_i
      if ( report_year < current_year + 1) and (report_year > current_year -2)
        delta  = report_year * 12 + current_month  - current_year * 12 - report_month
        if(delta < 12) and (delta >= 0)
          gigs_report[delta] = report.gigs
          likes_report[delta] = report.likes
          tips_report[delta] = report.tips
        end
      end
    end
  	{
  		:id => id,
  		:name => name,
  		:title => title,
  		:about => about,
  		:bio => bio,
  		:fbID => fbID,
  		:statsGigs => statsGigs,
  		:statsTips => statsTips,
  		:statsRequests => statsRequests,
  		:statsLikes => statsLikes,
  		:statsReviews => statsReviews,
  		:instagramURLString => instagramURLString,
  		:twitterURLString => twitterURLString,
  		:facebookURLString => facebookURLString,
  		:isVerified => isVerified,
  		:email => email,
  		:user => user.as_json,
  		:city => city_id,
  		:followersCount => self[:followers_count],
  		:cover_photo => (cover_photo.nil? ? nil : cover_photo.as_json),
  		:djType => dj_type.id,
  	  :musicType => dj_music_relations.pluck(:music_type_id),
  	  :vibeType => dj_vibe_relations.pluck(:vibe_type_id),
  		:created_at => created_at,
  		:updated_at => updated_at,
  		:reportGigs => gigs_report,
  		:reportLikes => likes_report,
  		:reportTips => tips_report
  	}
  end
  def as_brief_json(options={})
    	{
  		:id => id,
  		:name => name,
  		:title => title,
  		:about => about,
  		:cover_photo => (cover_photo.nil? ? nil : cover_photo.as_json),
    	:soundCloudURL => (self.user.nil? ? nil : self.user[:soundCloudURL])
    	}
  end
  #
  # Constant declare for stripe payment
  #
  ADMIN_FEE                = Rails.application.secrets.stripe_admin_fee_per
  STRIPE_ACCOUNT_ID        = Rails.application.secrets.stripe_account_id
  def pay_with_card(stripe_details, current_user)
    begin
      #
      # Stripe Secret Keys 'Dont share this key'
      #
      Stripe.api_key                  = Rails.application.secrets.stripe_api_key
      
      #
      # Card Details
      #
      card_number      = stripe_details[:number] rescue nil 
      card_exp_month   = stripe_details[:exp_month] rescue nil
      card_exp_year    = stripe_details[:exp_year] rescue nil
      card_cvc         = stripe_details[:cvc] rescue nil
      tip_to_dj_user   = stripe_details[:tip] rescue nil
      
      #
      # Money Deduction
      #
      tip_in_cents     = dollar_to_cents(tip_to_dj_user)
      admin_charges    = tip_in_cents / 100 * ADMIN_FEE
    
      #
      # Token to verify card details
      #
      user_token = Stripe::Token.create(
        card: {
                number:    card_number,
                exp_month: card_exp_month,
                exp_year:  card_exp_year,
                cvc:       card_cvc
              }
      )
      
      #
      # Charging process
      # direct money transfer to admin
      charge = Stripe::Charge.create({
        source:          user_token.id,
        amount:          tip_in_cents - admin_charges,
        description:     "DJ tip",
        currency:        'usd',
        destination: {
          amount:  admin_charges,
          account: STRIPE_ACCOUNT_ID
        }
      })
      self.tip_offs.create(user_id: current_user.id, amount: cents_to_dollar(charge.amount)) if charge[:paid]
      Rails.logger.info("Stripe transaction for #{self.email} given by #{current_user.email}") and return true if charge[:paid]
    rescue Stripe::InvalidRequestError => e
      self.errors[:base] << e.message
      return false
    rescue Stripe::CardError => e
      self.errors[:base] << e.message
      return false
    end
  end
  
  def dollar_to_cents(dollar)
    (100 * dollar.to_r).to_i
  end
  
  def cents_to_dollar(cents)
    (cents / 100)
  end
  
  def post_review(review_details, current_user)
    begin
      if Review.where(user_id: current_user.id).where(dj_user_id: self.id).size == 0
         review = Review.create(user_id: current_user.id, dj_user_id: self.id, content: review_details["content"], stars: review_details["stars"] )
      else 
         return 1 
      end
    rescue 
        return -1
    end
  end
  
  def devise_token
    if self.user != nil
      self.user.devise_token
    else
      nil
    end
  end
end
