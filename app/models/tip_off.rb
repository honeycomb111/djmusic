# == Schema Information
#
# Table name: tip_offs
#
#  id         :integer          not null, primary key
#  dj_user_id :integer
#  user_id    :integer
#  amount     :decimal(10, 2)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_tip_offs_on_dj_user_id  (dj_user_id)
#  index_tip_offs_on_user_id     (user_id)
#

class TipOff < ApplicationRecord
  belongs_to :dj_user
  belongs_to :user
  after_save :update_tips
  
  def update_tips
    event_period = DateTime.now.strftime("%Y-%m")
    report = Report.where(dj_user_id: self.dj_user_id).where(period: event_period)
  
    if report.size == 0 
      Report.create(dj_user_id:  self.dj_user_id, period: event_period, tips: amount)
    else
      report = report.first
      tips = report.tips.to_i + self.amount
      report.update_column(:tips, tips )
     
    end
  end
  
  def as_json
     user = User.find(self.user_id)
     {:amount => amount,
      :created_at => created_at, 
      :user =>  user.nil? ? {} : user.as_brief_json
     }
  end
  
end
