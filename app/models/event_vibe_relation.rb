# == Schema Information
#
# Table name: event_vibe_relations
#
#  id           :integer          not null, primary key
#  event_id     :integer
#  vibe_type_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_event_vibe_relations_on_event_id                   (event_id)
#  index_event_vibe_relations_on_event_id_and_vibe_type_id  (event_id,vibe_type_id) UNIQUE
#  index_event_vibe_relations_on_vibe_type_id               (vibe_type_id)
#

class EventVibeRelation < ApplicationRecord
  belongs_to :vibe_type
  belongs_to :event
  
  validates_uniqueness_of :id, scope: %i[event_id vibe_type_id]
end
