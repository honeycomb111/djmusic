# == Schema Information
#
# Table name: bar_music_relations
#
#  id            :integer          not null, primary key
#  bar_id        :integer
#  music_type_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_bar_music_relations_on_bar_id                    (bar_id)
#  index_bar_music_relations_on_bar_id_and_music_type_id  (bar_id,music_type_id) UNIQUE
#  index_bar_music_relations_on_music_type_id             (music_type_id)
#
# Foreign Keys
#
#  fk_rails_...  (bar_id => bars.id)
#  fk_rails_...  (music_type_id => music_types.id)
#

class BarMusicRelation < ApplicationRecord
  belongs_to :music_type
  belongs_to :bar
  
  validates_uniqueness_of :id, scope: %i[bar_id music_type_id]
end
