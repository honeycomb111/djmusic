# == Schema Information
#
# Table name: dj_music_relations
#
#  id            :integer          not null, primary key
#  dj_user_id    :integer
#  music_type_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_dj_music_relations_on_dj_user_id                    (dj_user_id)
#  index_dj_music_relations_on_dj_user_id_and_music_type_id  (dj_user_id,music_type_id) UNIQUE
#  index_dj_music_relations_on_music_type_id                 (music_type_id)
#

class DjMusicRelation < ApplicationRecord
  belongs_to :music_type
  belongs_to :dj_user
  
  # validates_uniqueness_of :index_dj_music_relations_on_dj_user_id_and_music_type_id, scope: %i[dj_user_id music_type_id]
end
