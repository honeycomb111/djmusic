# == Schema Information
#
# Table name: events
#
#  id               :integer          not null, primary key
#  title            :string
#  longDescription  :text
#  shortDescription :text
#  latitude         :float
#  longitude        :float
#  restrictAge      :integer
#  attendingCount   :integer
#  interestedCount  :integer
#  isCanceled       :integer
#  isFree           :integer
#  fbLink           :string
#  city_id          :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  dj_user_id       :integer
#  cover_photo_id   :integer
#  openingTime      :datetime
#  endTime          :datetime
#  hostedBy         :string
#  ticketPrice      :float
#
# Indexes
#
#  index_events_on_dj_user_id  (dj_user_id)
#

class Event < ApplicationRecord
  
  validates_uniqueness_of :title
	belongs_to :city, optional: true
	# belongs_to :music_type, optional: true

  belongs_to :dj_user, optional: false
  belongs_to :cover_photo, optional: true
  has_many :songs, :dependent => :destroy
  
  has_many :event_music_relations
  has_many :music_types , through: :event_music_relations
  
  has_many :event_vibe_relations 
  has_many :vibe_types , through: :event_vibe_relations
  
  validates :music_types, :presence => true
  validates :vibe_types, :presence => true
  
  acts_as_followable
  
  enum restrictAge: {"Age18"=>18, "Age21"=>21, "All"=>0} 
  after_save :update_report
  
  after_initialize :init
  
  def init
      self.isCanceled  ||= 0           #will set the default value only if it's nil
      self.isFree ||= 0 #let's you set a default association
    end
  def as_json(options={})
  	{
  		:id => id,
  		:title => title,
  		:longDescription => longDescription,
  		:shortDescription => shortDescription,
  		:hostedBy => hostedBy,
  		:latitude => latitude,
  		:longitude => longitude,
  		:restrictAge => Event.restrictAges[restrictAge],
  		:attendingCount => attendingCount,
  		:interestedCount => interestedCount,
  		:isCanceled => isCanceled,
  		:isFree => isFree,
  		:fbLink => fbLink,
  		:city => city_id,
  	  :musicType => event_music_relations.pluck(:music_type_id),
  	  :vibeType => event_vibe_relations.pluck(:vibe_type_id),		
  		:cover_photo => (cover_photo.nil? ? {} : cover_photo.as_json),
  		:dj_user => dj_user.nil? ? {} : dj_user.as_brief_json,
  		:openingTime => openingTime,
  		:endTime => endTime,
  		:ticketPrice => ticketPrice,
  		:created_at => created_at,
  		:updated_at => updated_at
  	}
  end
  
  def update_report
    update_gigs
  end
  
  def update_gigs
 
    if self.openingTime.nil? == false 
      event_period = self.openingTime.strftime("%Y-%m")
      report = Report.where(dj_user_id: self.dj_user_id).where(period: event_period)
      if report.size == 0 
        Report.create(dj_user_id: self.dj_user_id, period: event_period, gigs: 1)
      else
        report = report.first
        gigs = report.gigs.to_i + 1
        report.update_column(:gigs, gigs )
       
      end
    end
    
  end
  def city_name
   self.city_id.nil? ? "" : City.find(self.city_id).name
   
  end
end
