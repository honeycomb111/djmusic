# == Schema Information
#
# Table name: identities
#
#  id           :integer          not null, primary key
#  fb_id        :string
#  access_token :string
#  user_id      :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Identity < ApplicationRecord
  belongs_to :user
  validates :fb_id, :access_token, :presence => true
end
