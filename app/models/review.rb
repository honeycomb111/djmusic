# == Schema Information
#
# Table name: reviews
#
#  id         :integer          not null, primary key
#  content    :string
#  stars      :integer
#  dj_user_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Indexes
#
#  index_reviews_on_dj_user_id  (dj_user_id)
#

class Review < ApplicationRecord
  belongs_to :dj_user
  def as_json(options={})
    user = User.find_by_id(user_id);
    {
  		:content => content,
  		:stars => stars,
  		:createdAt => created_at,
        :user => {name: user.name,
                 cover_photo: (user.cover_photo.nil? ? {} : user.cover_photo.as_json)}
   	}
  end
end
