# == Schema Information
#
# Table name: activities
#
#  id            :integer          not null, primary key
#  activity_name :string
#  user_id       :integer
#  dj_user_id    :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Activity < ApplicationRecord
  # belongs_to :dj_user
  # belongs_to :user
    def as_json(options={})
    {
        :id => id,
        :activity_name => activity_name,
        :user => (user_id.nil? ? nil :User.find(user_id).as_brief_json),  
        :djUser =>(dj_user_id.nil? ? nil :DjUser.find(dj_user_id).as_brief_json),
        :created_at => created_at
    }
    end
end
