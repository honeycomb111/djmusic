# == Schema Information
#
# Table name: bars
#
#  id                       :integer          not null, primary key
#  rating                   :integer
#  price                    :string
#  phone                    :string
#  is_closed                :boolean
#  categories_alias         :string
#  categories_title         :string
#  review_count             :integer
#  name                     :string
#  url                      :string
#  lat                      :float
#  long                     :float
#  image_url                :string
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  bar_id                   :string
#  display_phone            :string
#  address1                 :string
#  address2                 :string
#  address3                 :string
#  city                     :string
#  zip_code                 :string
#  country                  :string
#  state                    :string
#  display_address          :string
#  city_id                  :integer
#  hours_type               :string
#  is_overnight             :boolean
#  bar_end                  :string
#  day                      :string
#  start                    :string
#  is_credits_cards         :boolean
#  is_apple_pay             :boolean
#  is_android_pay           :boolean
#  is_bike_parking          :boolean
#  is_wheelchair_accessible :boolean
#  is_good_for_groups       :boolean
#  is_good_for_dancing      :boolean
#  happy_hour               :string
#  parking                  :string
#  ambience                 :string
#  noise_level              :string
#  music                    :string
#  alchole                  :string
#  best_nights              :string
#  age_allowed              :string
#
# Indexes
#
#  index_bars_on_bar_id_and_lat_and_long  (bar_id,lat,long)
#  index_bars_on_city_id                  (city_id)
#
# Foreign Keys
#
#  fk_rails_...  (city_id => cities.id)
#

class Bar < ApplicationRecord
    belongs_to :city
    attr_accessor :distance 
    
    has_many :bar_music_relations
    has_many :music_types , through: :bar_music_relations
    
    has_many :bar_vibe_relations
    has_many :vibe_types , through: :bar_vibe_relations
    def as_json(options = { })
        h = super(options)
        h[:distance]  = distance.nil? ? nil :distance.round(2)
        h[:musicType] = bar_music_relations.pluck(:music_type_id)
  	    h[:vibeType]  = bar_vibe_relations.pluck(:vibe_type_id)
        h
    end
    def get_distance loc1
      if self.lat == nil or self.long == nil 
        return -1 
      else
          loc2 = [self.lat, self.long]
          rad_per_deg = Math::PI/180  # PI / 180
          rkm = 6371                  # Earth radius in kilometers
          rm = rkm * 1000             # Radius in meters
          
          dlat_rad = (loc2[0] - loc1[0]) * rad_per_deg  # Delta, converted to rad
          dlon_rad = (loc2[1] - loc1[1]) * rad_per_deg
        
          lat1_rad, lon1_rad = loc1.map {|i| i * rad_per_deg }
          lat2_rad, lon2_rad = loc2.map {|i| i * rad_per_deg }
        
          a = Math.sin(dlat_rad/2)**2 + Math.cos(lat1_rad) * Math.cos(lat2_rad) * Math.sin(dlon_rad/2)**2
          c = 2 * Math::atan2(Math::sqrt(a), Math::sqrt(1-a))
        
          return rm * c # Delta in meters
      end
    end
end
