# == Schema Information
#
# Table name: dj_vibe_relations
#
#  id           :integer          not null, primary key
#  dj_user_id   :integer
#  vibe_type_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_dj_vibe_relations_on_dj_user_id                   (dj_user_id)
#  index_dj_vibe_relations_on_dj_user_id_and_vibe_type_id  (dj_user_id,vibe_type_id) UNIQUE
#  index_dj_vibe_relations_on_vibe_type_id                 (vibe_type_id)
#

class DjVibeRelation < ApplicationRecord
  belongs_to :vibe_type
  belongs_to :dj_user
  
  validates_uniqueness_of :id, scope: %i[dj_user_id vibe_type_id]
end
