# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  name                   :string
#  title                  :string
#  birthday               :date
#  about                  :text
#  facebookURLString      :string
#  cover_photo_id         :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#  authentication_token   :string(30)
#  followees_count        :integer          default(0)
#  dj_user_id             :integer
#  devise_token           :string
#  soundCloudURL          :string
#  city_id                :integer
#
# Indexes
#
#  index_users_on_authentication_token  (authentication_token) UNIQUE
#  index_users_on_cover_photo_id        (cover_photo_id)
#  index_users_on_dj_user_id            (dj_user_id)
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

# Foreign Keys
#
#  fk_rails_...  (cover_photo_id => cover_photos.id)
#

class User < ApplicationRecord
  
  validates_uniqueness_of :email
	acts_as_token_authenticatable
	acts_as_follower
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  
  belongs_to :cover_photo, optional: true
	has_one :identity, :dependent => :destroy
	has_many :tip_offs, dependent: :destroy
  has_many :songs
  belongs_to :city, optional: true
  # has_many :activities, dependent: :destroy
  #has_many :chats, dependent: :destroy
  has_one :dj_user
  validates :name,  presence: true
  validates :email, uniqueness: true, on: :create
  def as_json(options={})
  	{
  		:id => id,
  		:name => name,
  		:title => title,
  	  :birthday => birthday,
  		:about => about,
  		:facebookURLString => facebookURLString,
  		:soundCloudURLString => soundCloudURL,
  		:cover_photo => (cover_photo.nil? ? {} : cover_photo.as_json),
  	  :email => email,
  		:followees_count => followees_count,
  		:djUser => (self.dj_user.nil? ? nil: self.dj_user.id),
  		:created_at => created_at,
  		:city_id => city_id,
  		# :dj_user_id => dj_user_id
  		# :updated_at => updated_at
  	}
  end
  def as_brief_json()
    { 
      :id => id,
      :name => name,
  		:title => title,
  		:cover_photo => (cover_photo.nil? ? {} : cover_photo.as_json)
    }
  end
  

  
  def followees_event
     self.follows.map{|data|  data.followable_id if data.followable_type = 'Event'}
  end
end
