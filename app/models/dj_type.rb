# == Schema Information
#
# Table name: dj_types
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class DjType < ApplicationRecord
    has_many  :dj_users, dependent: :destroy
  	accepts_nested_attributes_for   :dj_users
end
