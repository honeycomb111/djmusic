# == Schema Information
#
# Table name: cover_photos
#
#  id                  :integer          not null, primary key
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  source_file_name    :string
#  source_content_type :string
#  source_file_size    :integer
#  source_updated_at   :datetime
#  xOffset             :integer          default(0)
#  yOffset             :integer          default(0)
#  Float               :integer          default(0)
#  webLink             :string
#

class CoverPhoto < ApplicationRecord
  has_attached_file :source, styles: {thumb: "100x100>", medium: "300x300>", :original => "2208×2208>"  },
    :path => ":rails_root/public/coverphotos/:id/:basename_:style.:extension",
    :url  => "/coverphotos/:id/:basename_:style.:extension", default_url: "/images/missing.png"
  #   :default_style => :original

  
  # validates_attachment_content_type :source, content_type: /\Aimage\/.*\z/
  do_not_validate_attachment_file_type :source   
   
  has_one :user, dependent: :destroy
  accepts_nested_attributes_for   :user

  has_one :dj_user, dependent: :destroy
  accepts_nested_attributes_for   :dj_user

  has_one :event, dependent: :destroy
  accepts_nested_attributes_for   :event
  attr_accessor :web_url
  
  validates :source, presence: true
  before_save  :web_url_checker  
  def web_url_checker
    # unless (type = remote_file_type(webLink)).nil?
    #   b = Base64.strict_encode64(open(webLink) { |io| io.read })
    #   binding.pry
    if webLink != nil
      image_file                   = Paperclip.io_adapters.for(webLink)
      image_file.original_filename = "photo"
      # image_file.content_type      = "type"
      self.source          = image_file
    end
    # end
  end
    
require 'base64'
  
  def as_json(options={})
  	{
  		:id => id,
  		:xOffset => xOffset,
  		:yOffset => yOffset,
  		:Float => self.Float,
  		:source => {
  			:original =>  "#{APP_URL}#{source.url}",
  			:medium => "#{APP_URL}#{source.url(:medium)}",
  			:thumb => "#{APP_URL}#{source.url(:thumb)}"
  		}
  	}
  end
  
  def web_url
    # "#{self.label} #{self.another_column} #{self.another_column2}"
   
    # ActiveSupport::Base64.encode64(open("http://image.com/img.jpg") { |io| io.read })
  end
  # def remote_file_type(webLink)
  #   unless webLink.nil? || webLink == ""
  #     url = URI.encode(webLink)
  #     url = URI.parse(url)
  #     http = Net::HTTP.new(url.host, url.port)
  #     if url.scheme == "https"
  #         http.use_ssl = true
  #     end
  #     if http.head(url.request_uri)['Content-Type'].start_with? 'image'
  #       return http.head(url.request_uri)['Content-Type']
  #     else
  #       return nil
  #     end
   
  #   end
  #   return nil
  # end
end
