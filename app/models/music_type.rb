# == Schema Information
#
# Table name: music_types
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class MusicType < ApplicationRecord
    
  has_many :dj_music_relations
  has_many :dj_users , through: :dj_music_relations
  
  has_many :event_music_relations
  has_many :events , through: :event_music_relations 
  
  
  has_many :bar_music_relations
  has_many :bars , through: :bar_music_relations
end
