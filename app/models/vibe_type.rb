# == Schema Information
#
# Table name: vibe_types
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class VibeType < ApplicationRecord
  has_many :dj_vibe_relations
  has_many :dj_users , through: :dj_vibe_relations
  
  has_many :event_vibe_relations
  has_many :events , through: :event_vibe_relations 
  
  has_many :bar_vibe_relations
  has_many :bars , through: :bar_vibe_relations 
end
