json.activities @activities do |activity|
  json.id activity.id
  json.activity_name activity.activity_name
end