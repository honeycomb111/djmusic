json.bars @bars do |bar|
  json.id bar.id
  json.name bar.name
  json.image_url bar.image_url
  json.is_closed bar.is_closed
  json.url bar.url
  json.review_count bar.review_count
  json.categories{
     json.categories_alias bar.categories_alias
     json.categories_title bar.categories_title
  }
  json.rating bar.rating  

  json.coordinates {
    json.latitude bar.lat
    json.longitude bar.long
  }
  json.price bar.price
  json.location{
    json.address1 bar.address1 
    json.address2 bar.address2 
    json.address3 bar.address3
    json.city bar.city
    json.zip_code bar.zip_code
    json.country bar.country
    json.state bar.state
    json.display_address{
      json.address bar.address1 + " " + " " + bar.city + " " + bar.country + " " + bar.zip_code
    }
  }
  json.phone bar.phone
  json.display_phone bar.display_phone
  json.distance bar.distance
end