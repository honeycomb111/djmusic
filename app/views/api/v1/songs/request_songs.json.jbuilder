json.songs @songs do |song|
  json.song_name song.name
  json.artist_name song.artist_name
end