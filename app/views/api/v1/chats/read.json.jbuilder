json.chats @chat do |chat|
  json.id chat.id
  json.message chat.message
  json.sender_id chat.sender_id
  json.sender_name User.find(chat.sender_id).name
  json.reciever_id chat.reciever_id
  json.reciever_name User.find(chat.reciever_id).name
  json.date chat.created_at.strftime("%F %I:%M %p")
  json.status 200
end