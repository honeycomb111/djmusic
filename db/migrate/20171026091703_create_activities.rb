class CreateActivities < ActiveRecord::Migration[5.1]
  def change
    create_table :activities do |t|
      t.string :activity_name
      t.integer :user_id
      t.integer :dj_user_id

      t.timestamps
    end
    # add_index :activities, [ :user_id, :dj_user_id ]
  end
end
