class CreateReviews < ActiveRecord::Migration[5.1]
  def change
    create_table :reviews do |t|
      t.string :content
      t.integer :stars
      t.belongs_to :dj_user
      t.timestamps
    end
  end
end
