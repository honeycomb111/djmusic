class CreateIdentities < ActiveRecord::Migration[5.1]
  def change
    create_table :identities do |t|
      t.string :fb_id
      t.string :access_token
      t.integer :user_id

      t.timestamps
    end
  end
end
