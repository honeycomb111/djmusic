class ChangeDatatypeForStatus < ActiveRecord::Migration[5.1]
  def change
     remove_column :songs, :status, :boolean
     add_column :songs, :status, :integer, default: 0
  end
end
