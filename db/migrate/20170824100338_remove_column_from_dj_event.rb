class RemoveColumnFromDjEvent < ActiveRecord::Migration[5.1]
  def change
  	remove_column :events, :cover_photo_id
  	remove_column :dj_users, :cover_photo_id
  	add_column :dj_users, :user_id, :integer
  end
end
