class CreateCoverPhotos < ActiveRecord::Migration[5.1]
  def change
    create_table :cover_photos do |t|
      t.float :xOffset
      t.string :yOffset
      t.string :Float
      t.string :source

      t.timestamps
    end
  end
end
