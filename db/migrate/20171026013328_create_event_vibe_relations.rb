class CreateEventVibeRelations < ActiveRecord::Migration[5.1]
  def change
    create_table :event_vibe_relations do |t|
      t.references :event, foreign_key: true
      t.references :vibe_type, foreign_key: true

      t.timestamps
    end
    add_index :event_vibe_relations, [ :event_id, :vibe_type_id ], :unique => true
  end
end
