class RemoveMusicTypeIdFromEvent < ActiveRecord::Migration[5.1]
  def change
    remove_column :events, :music_type_id, :integer
    remove_column :events, :vibe_type_id, :integer
  end
end
