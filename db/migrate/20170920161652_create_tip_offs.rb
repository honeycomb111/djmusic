class CreateTipOffs < ActiveRecord::Migration[5.1]
  def change
    create_table :tip_offs do |t|
      t.references :dj_user, foreign_key: true
      t.references :user, foreign_key: true
      t.decimal :amount, precision: 10, scale: 2

      t.timestamps
    end
  end
end
