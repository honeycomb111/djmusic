class CreateDjMusicRelations < ActiveRecord::Migration[5.1]
  def change
    create_table :dj_music_relations do |t|

      t.references :dj_user, foreign_key: true
      t.references :music_type, foreign_key: true

      t.timestamps
    end
    add_index :dj_music_relations, [ :dj_user_id, :music_type_id ], :unique => true
  end
end
