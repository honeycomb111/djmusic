class CreateDjUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :dj_users do |t|
      t.string :name
      t.string :email
      t.string :title
      t.text :about
      t.integer :type
      t.string :fbID
      t.integer :statsGigs
      t.integer :statsTips
      t.integer :statsRequests
      t.integer :statsLikes
      t.integer :statsReviews
      t.string :instagramURLString
      t.string :twitterURLString
      t.string :facebookURLString
      t.boolean :isVerified
      t.references :cover_photo, foreign_key: true

      t.timestamps
    end
  end
end
