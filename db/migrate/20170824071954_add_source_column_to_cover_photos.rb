class AddSourceColumnToCoverPhotos < ActiveRecord::Migration[5.1]
  def self.up
    change_table :cover_photos do |t|
      t.attachment :source
    end
  end

  def self.down
    remove_attachment :cover_photos, :source
  end
end
