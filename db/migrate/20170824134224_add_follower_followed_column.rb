class AddFollowerFollowedColumn < ActiveRecord::Migration[5.1]
  def change
  	add_column :users, :followees_count, :integer, :default => 0
  	add_column :dj_users, :followers_count, :integer, :default => 0
  end
end
