class AddTicketPriceToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :ticketPrice, :float
  end
end
