class AddDisplayAddressToBar < ActiveRecord::Migration[5.1]
  def change
      add_column :bars, :display_address, :string
  end
end
