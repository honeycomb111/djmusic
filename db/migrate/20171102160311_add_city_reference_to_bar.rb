class AddCityReferenceToBar < ActiveRecord::Migration[5.1]
  def change
     add_reference :bars, :city, foreign_key: true
  end
end
