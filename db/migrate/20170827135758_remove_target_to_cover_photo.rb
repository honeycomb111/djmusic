class RemoveTargetToCoverPhoto < ActiveRecord::Migration[5.1]
  def change
  	remove_column :cover_photos, :target_id
  	remove_column :cover_photos, :target_type
  end
end
