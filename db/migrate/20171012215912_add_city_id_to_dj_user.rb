class AddCityIdToDjUser < ActiveRecord::Migration[5.1]
  def change
    add_reference :dj_users, :city, foreign_key: true
  end
end
