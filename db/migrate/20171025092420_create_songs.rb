class CreateSongs < ActiveRecord::Migration[5.1]
  def change
    create_table :songs do |t|
      t.string :name
      t.string :artist_name
      t.references :event, foreign_key: true
      t.references :dj_user, foreign_key: true
      t.references :user, foreign_key: true
      t.boolean :status, default: false

      t.timestamps
    end
    add_index :songs, [ :event_id, :dj_user_id, :user_id ]
  end
end
