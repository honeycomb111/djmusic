class CreateDjVibeRelations < ActiveRecord::Migration[5.1]
  def change
    create_table :dj_vibe_relations do |t|
      t.references :dj_user, foreign_key: true
      t.references :vibe_type, foreign_key: true

      t.timestamps
    end
    add_index :dj_vibe_relations, [ :dj_user_id, :vibe_type_id ], :unique => true
  end
end
