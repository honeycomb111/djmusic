class RemoveDjTypeColumnFromEvents < ActiveRecord::Migration[5.1]
  def change
    remove_column :events, :dj_type_id
  end
end
