class RenameDjUserColumnName < ActiveRecord::Migration[5.1]
  def up
  	rename_column :dj_users, :type, :dj_type
  end

  def down
  	rename_column :dj_users, :dj_type, :type
  end
end