class ChangeDatatypeForReceiver < ActiveRecord::Migration[5.1]
  def change
     remove_column :chats, :reciever_id, :integer
     add_column :chats, :receiver_id, :integer
  end
end
