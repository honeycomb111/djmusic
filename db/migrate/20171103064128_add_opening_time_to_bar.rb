class AddOpeningTimeToBar < ActiveRecord::Migration[5.1]
  def change
    add_column :bars, :hours_type, :string
    add_column :bars, :is_overnight, :boolean
    add_column :bars, :bar_end, :string
    add_column :bars, :day, :string
    add_column :bars, :start, :string

  end
end
