class AddCoverPhotoIdToDjUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :dj_users, :cover_photo_id, :integer
  end
end
