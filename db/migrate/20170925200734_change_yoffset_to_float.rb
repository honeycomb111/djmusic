class ChangeYoffsetToFloat < ActiveRecord::Migration[5.1]
  def change
     remove_column :cover_photos, :yOffset, :float
     remove_column :cover_photos, :xOffset, :float
     remove_column :cover_photos, :Float, :float
     
     add_column :cover_photos, :xOffset, :integer, default: 0
     add_column :cover_photos, :yOffset, :integer, default: 0
     add_column :cover_photos, :Float, :integer, default: 0
    
  end
end
