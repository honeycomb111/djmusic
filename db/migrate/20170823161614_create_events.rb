class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :title
      t.text :longDescription
      t.text :shortDescription
      t.float :latitude
      t.float :longtitude
      t.integer :restrictAge
      t.time :openingTime
      t.time :endTime
      t.integer :attendingCount
      t.integer :interestedCount
      t.integer :isCanceled
      t.integer :isFree
      t.string :fbLink
      t.integer :city_id
      t.integer :music_type_id
      t.integer :vibe_type_id
      t.integer :dj_type_id
      t.integer :cover_photo_id

      t.timestamps
    end
  end
end
