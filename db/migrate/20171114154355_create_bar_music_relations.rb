class CreateBarMusicRelations < ActiveRecord::Migration[5.1]
  def change
    create_table :bar_music_relations do |t|
      t.references :bar, foreign_key: true
      t.references :music_type, foreign_key: true
      t.timestamps
    end
     add_index :bar_music_relations, [ :bar_id, :music_type_id ], :unique => true
  end
end
