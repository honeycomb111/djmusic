class AddDjUserToEvent < ActiveRecord::Migration[5.1]
  def change
    add_reference :events, :dj_user, foreign_key: true
  end
end
