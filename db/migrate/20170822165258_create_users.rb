class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :title
      t.date :birthday
      t.text :about
      t.string :facebookURLString
      t.string :pwd
      t.references :cover_photo, foreign_key: true
      t.timestamps
    end
  end
end
