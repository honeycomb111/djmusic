class RemoveEventIdFromMusicType < ActiveRecord::Migration[5.1]
  def change
      remove_reference :music_types, :event
  end
end
