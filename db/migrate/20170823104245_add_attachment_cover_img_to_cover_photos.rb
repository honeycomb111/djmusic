class AddAttachmentCoverImgToCoverPhotos < ActiveRecord::Migration[5.1]
  def self.up
    change_table :cover_photos do |t|
      t.attachment :cover_img
    end
  end

  def self.down
    remove_attachment :cover_photos, :cover_img
  end
end
