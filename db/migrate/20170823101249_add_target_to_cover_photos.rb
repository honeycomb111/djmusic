class AddTargetToCoverPhotos < ActiveRecord::Migration[5.1]
  def change
    add_column :cover_photos, :target_id, :integer
    add_column :cover_photos, :target_type, :string
  end
end
