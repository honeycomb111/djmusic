class AddBioToDjUser < ActiveRecord::Migration[5.1]
  def change
    add_column :dj_users, :bio, :text
  end
end
