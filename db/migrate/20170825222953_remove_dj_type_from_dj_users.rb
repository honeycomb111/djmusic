class RemoveDjTypeFromDjUsers < ActiveRecord::Migration[5.1]
  def change
    remove_column :dj_users, :dj_type, :integer
  end
end
