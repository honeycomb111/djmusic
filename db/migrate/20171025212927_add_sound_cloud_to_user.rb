class AddSoundCloudToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :soundCloudURL, :string
  end
end
