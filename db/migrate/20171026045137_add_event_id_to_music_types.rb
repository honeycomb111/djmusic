class AddEventIdToMusicTypes < ActiveRecord::Migration[5.1]
  def change
    add_column :music_types, :event_id, :integer
    add_index :music_types, [ :event_id ]
  end

end
