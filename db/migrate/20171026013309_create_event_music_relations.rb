class CreateEventMusicRelations < ActiveRecord::Migration[5.1]
  def change
    create_table :event_music_relations do |t|
      t.references :event, foreign_key: true
      t.references :music_type, foreign_key: true

      t.timestamps
    end
    add_index :event_music_relations, [ :event_id, :music_type_id ], :unique => true
  end
end
