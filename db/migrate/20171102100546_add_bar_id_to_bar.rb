class AddBarIdToBar < ActiveRecord::Migration[5.1]
  def change
    add_column :bars, :bar_id, :string
    add_column :bars, :display_phone, :string
    add_column :bars, :distance, :string
    add_column :bars, :address1, :string
    add_column :bars, :address2, :string
    add_column :bars, :address3, :string
    add_column :bars, :city, :string
    add_column :bars, :zip_code, :string
    add_column :bars, :country, :string
    add_column :bars, :state, :string


    add_index :bars, [ :bar_id, :lat, :long ]
  end
end
