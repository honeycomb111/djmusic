class CreateReports < ActiveRecord::Migration[5.1]
  def change
    create_table :reports do |t|
      t.belongs_to :dj_user
      t.string :period
      t.integer :gigs, :default => 0
      t.integer :likes, :default => 0
      t.integer :tips, :default => 0
      t.timestamps
    end
  end
end
