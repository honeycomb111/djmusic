class AddCoverPhotoIdToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :cover_photo_id, :integer
  end
end
