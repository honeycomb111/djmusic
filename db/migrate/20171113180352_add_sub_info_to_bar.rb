class AddSubInfoToBar < ActiveRecord::Migration[5.1]
  def change
      
    add_column :bars, :is_credits_cards, :boolean
    add_column :bars, :is_apple_pay, :boolean
    add_column :bars, :is_android_pay, :boolean
    add_column :bars, :is_bike_parking, :boolean
    add_column :bars, :is_wheelchair_accessible, :boolean    
    add_column :bars, :is_good_for_groups, :boolean
    add_column :bars, :is_good_for_dancing, :boolean
    
    
    add_column :bars, :happy_hour, :string
    add_column :bars, :parking, :string   
    add_column :bars, :ambience, :string
    add_column :bars, :noise_level, :string
    add_column :bars, :music, :string
    add_column :bars, :alchole, :string
    add_column :bars, :best_nights, :string
    add_column :bars, :age_allowed, :string    
  end
end
