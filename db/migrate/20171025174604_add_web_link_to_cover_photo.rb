class AddWebLinkToCoverPhoto < ActiveRecord::Migration[5.1]
  def change
    add_column :cover_photos, :webLink, :string
  end
end
