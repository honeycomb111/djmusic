class RemoveAddColumnToCoverPhotos < ActiveRecord::Migration[5.1]
  def change
  	remove_attachment :cover_photos, :cover_img
  	remove_column :cover_photos, :source
  end
end
