class CreateBarVibeRelations < ActiveRecord::Migration[5.1]
  def change
    create_table :bar_vibe_relations do |t|
      t.references :bar, foreign_key: true
      t.references :vibe_type, foreign_key: true
      t.timestamps
    end
    add_index :bar_vibe_relations, [ :bar_id, :vibe_type_id ], :unique => true
  end
end


