class CreateBars < ActiveRecord::Migration[5.1]
  def change
    create_table :bars do |t|
      t.integer :rating
      t.string :price
      t.string :phone
      t.boolean :is_closed
      t.string :categories_alias
      t.string :categories_title
      t.integer :review_count
      t.string :name
      t.string :url
      t.float :lat
      t.float :long
      t.string :image_url

      t.timestamps
    end
  end
end
