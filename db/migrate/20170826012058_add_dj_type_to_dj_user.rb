class AddDjTypeToDjUser < ActiveRecord::Migration[5.1]
  def change
    add_reference :dj_users, :dj_type, foreign_key: true
  end
end
