class ChangeOpeningEndTimeColumntypeToEvents < ActiveRecord::Migration[5.1]
  def change
    remove_column :events, :openingTime
    remove_column :events, :endTime
    add_column :events, :openingTime, :datetime
    add_column :events, :endTime, :datetime
  end
end
