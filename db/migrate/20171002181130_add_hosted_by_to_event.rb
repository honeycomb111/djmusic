class AddHostedByToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :hostedBy, :string
  end
end
