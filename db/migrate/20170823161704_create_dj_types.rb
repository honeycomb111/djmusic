class CreateDjTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :dj_types do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
