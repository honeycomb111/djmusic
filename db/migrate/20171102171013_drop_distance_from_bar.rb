class DropDistanceFromBar < ActiveRecord::Migration[5.1]
  def change
      remove_column :bars, :distance
  end
end
