# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171114155507) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.string "activity_name"
    t.integer "user_id"
    t.integer "dj_user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bar_music_relations", force: :cascade do |t|
    t.bigint "bar_id"
    t.bigint "music_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["bar_id", "music_type_id"], name: "index_bar_music_relations_on_bar_id_and_music_type_id", unique: true
    t.index ["bar_id"], name: "index_bar_music_relations_on_bar_id"
    t.index ["music_type_id"], name: "index_bar_music_relations_on_music_type_id"
  end

  create_table "bar_vibe_relations", force: :cascade do |t|
    t.bigint "bar_id"
    t.bigint "vibe_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["bar_id", "vibe_type_id"], name: "index_bar_vibe_relations_on_bar_id_and_vibe_type_id", unique: true
    t.index ["bar_id"], name: "index_bar_vibe_relations_on_bar_id"
    t.index ["vibe_type_id"], name: "index_bar_vibe_relations_on_vibe_type_id"
  end

  create_table "bars", force: :cascade do |t|
    t.integer "rating"
    t.string "price"
    t.string "phone"
    t.boolean "is_closed"
    t.string "categories_alias"
    t.string "categories_title"
    t.integer "review_count"
    t.string "name"
    t.string "url"
    t.float "lat"
    t.float "long"
    t.string "image_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "bar_id"
    t.string "display_phone"
    t.string "address1"
    t.string "address2"
    t.string "address3"
    t.string "city"
    t.string "zip_code"
    t.string "country"
    t.string "state"
    t.string "display_address"
    t.bigint "city_id"
    t.string "hours_type"
    t.boolean "is_overnight"
    t.string "bar_end"
    t.string "day"
    t.string "start"
    t.boolean "is_credits_cards"
    t.boolean "is_apple_pay"
    t.boolean "is_android_pay"
    t.boolean "is_bike_parking"
    t.boolean "is_wheelchair_accessible"
    t.boolean "is_good_for_groups"
    t.boolean "is_good_for_dancing"
    t.string "happy_hour"
    t.string "parking"
    t.string "ambience"
    t.string "noise_level"
    t.string "music"
    t.string "alchole"
    t.string "best_nights"
    t.string "age_allowed"
    t.index ["bar_id", "lat", "long"], name: "index_bars_on_bar_id_and_lat_and_long"
    t.index ["city_id"], name: "index_bars_on_city_id"
  end

  create_table "chats", force: :cascade do |t|
    t.string "message"
    t.integer "sender_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "receiver_id"
  end

  create_table "cities", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cover_photos", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "source_file_name"
    t.string "source_content_type"
    t.integer "source_file_size"
    t.datetime "source_updated_at"
    t.integer "xOffset", default: 0
    t.integer "yOffset", default: 0
    t.integer "Float", default: 0
    t.string "webLink"
  end

  create_table "dj_music_relations", force: :cascade do |t|
    t.integer "dj_user_id"
    t.integer "music_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["dj_user_id", "music_type_id"], name: "index_dj_music_relations_on_dj_user_id_and_music_type_id", unique: true
    t.index ["dj_user_id"], name: "index_dj_music_relations_on_dj_user_id"
    t.index ["music_type_id"], name: "index_dj_music_relations_on_music_type_id"
  end

  create_table "dj_types", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "dj_users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "title"
    t.text "about"
    t.string "fbID"
    t.integer "statsGigs"
    t.integer "statsTips"
    t.integer "statsRequests"
    t.integer "statsLikes"
    t.integer "statsReviews"
    t.string "instagramURLString"
    t.string "twitterURLString"
    t.string "facebookURLString"
    t.boolean "isVerified"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.integer "followers_count", default: 0
    t.integer "dj_type_id"
    t.integer "cover_photo_id"
    t.integer "city_id"
    t.text "bio"
    t.index ["city_id"], name: "index_dj_users_on_city_id"
    t.index ["dj_type_id"], name: "index_dj_users_on_dj_type_id"
  end

  create_table "dj_vibe_relations", force: :cascade do |t|
    t.integer "dj_user_id"
    t.integer "vibe_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["dj_user_id", "vibe_type_id"], name: "index_dj_vibe_relations_on_dj_user_id_and_vibe_type_id", unique: true
    t.index ["dj_user_id"], name: "index_dj_vibe_relations_on_dj_user_id"
    t.index ["vibe_type_id"], name: "index_dj_vibe_relations_on_vibe_type_id"
  end

  create_table "event_music_relations", force: :cascade do |t|
    t.integer "event_id"
    t.integer "music_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id", "music_type_id"], name: "index_event_music_relations_on_event_id_and_music_type_id", unique: true
    t.index ["event_id"], name: "index_event_music_relations_on_event_id"
    t.index ["music_type_id"], name: "index_event_music_relations_on_music_type_id"
  end

  create_table "event_vibe_relations", force: :cascade do |t|
    t.integer "event_id"
    t.integer "vibe_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id", "vibe_type_id"], name: "index_event_vibe_relations_on_event_id_and_vibe_type_id", unique: true
    t.index ["event_id"], name: "index_event_vibe_relations_on_event_id"
    t.index ["vibe_type_id"], name: "index_event_vibe_relations_on_vibe_type_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "title"
    t.text "longDescription"
    t.text "shortDescription"
    t.float "latitude"
    t.float "longitude"
    t.integer "restrictAge"
    t.integer "attendingCount"
    t.integer "interestedCount"
    t.integer "isCanceled"
    t.integer "isFree"
    t.string "fbLink"
    t.integer "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "dj_user_id"
    t.integer "cover_photo_id"
    t.datetime "openingTime"
    t.datetime "endTime"
    t.string "hostedBy"
    t.float "ticketPrice"
    t.index ["dj_user_id"], name: "index_events_on_dj_user_id"
  end

  create_table "follows", force: :cascade do |t|
    t.string "follower_type"
    t.integer "follower_id"
    t.string "followable_type"
    t.integer "followable_id"
    t.datetime "created_at"
    t.index ["followable_id", "followable_type"], name: "fk_followables"
    t.index ["follower_id", "follower_type"], name: "fk_follows"
  end

  create_table "identities", force: :cascade do |t|
    t.string "fb_id"
    t.string "access_token"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "likes", force: :cascade do |t|
    t.string "liker_type"
    t.integer "liker_id"
    t.string "likeable_type"
    t.integer "likeable_id"
    t.datetime "created_at"
    t.index ["likeable_id", "likeable_type"], name: "fk_likeables"
    t.index ["liker_id", "liker_type"], name: "fk_likes"
  end

  create_table "mentions", force: :cascade do |t|
    t.string "mentioner_type"
    t.integer "mentioner_id"
    t.string "mentionable_type"
    t.integer "mentionable_id"
    t.datetime "created_at"
    t.index ["mentionable_id", "mentionable_type"], name: "fk_mentionables"
    t.index ["mentioner_id", "mentioner_type"], name: "fk_mentions"
  end

  create_table "music_types", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "reports", force: :cascade do |t|
    t.integer "dj_user_id"
    t.string "period"
    t.integer "gigs", default: 0
    t.integer "likes", default: 0
    t.integer "tips", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["dj_user_id"], name: "index_reports_on_dj_user_id"
  end

  create_table "reviews", force: :cascade do |t|
    t.string "content"
    t.integer "stars"
    t.integer "dj_user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.index ["dj_user_id"], name: "index_reviews_on_dj_user_id"
  end

  create_table "songs", force: :cascade do |t|
    t.string "name"
    t.string "artist_name"
    t.integer "event_id"
    t.integer "dj_user_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status", default: 0
    t.index ["dj_user_id"], name: "index_songs_on_dj_user_id"
    t.index ["event_id", "dj_user_id", "user_id"], name: "index_songs_on_event_id_and_dj_user_id_and_user_id"
    t.index ["event_id"], name: "index_songs_on_event_id"
    t.index ["user_id"], name: "index_songs_on_user_id"
  end

  create_table "tip_offs", force: :cascade do |t|
    t.integer "dj_user_id"
    t.integer "user_id"
    t.decimal "amount", precision: 10, scale: 2
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["dj_user_id"], name: "index_tip_offs_on_dj_user_id"
    t.index ["user_id"], name: "index_tip_offs_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "title"
    t.date "birthday"
    t.text "about"
    t.string "facebookURLString"
    t.integer "cover_photo_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "authentication_token", limit: 30
    t.integer "followees_count", default: 0
    t.integer "dj_user_id"
    t.string "devise_token"
    t.string "soundCloudURL"
    t.integer "city_id"
    t.index ["authentication_token"], name: "index_users_on_authentication_token", unique: true
    t.index ["cover_photo_id"], name: "index_users_on_cover_photo_id"
    t.index ["dj_user_id"], name: "index_users_on_dj_user_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "vibe_types", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "bar_music_relations", "bars"
  add_foreign_key "bar_music_relations", "music_types"
  add_foreign_key "bar_vibe_relations", "bars"
  add_foreign_key "bar_vibe_relations", "vibe_types"
  add_foreign_key "bars", "cities"
end
