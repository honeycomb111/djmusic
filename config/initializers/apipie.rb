Apipie.configure do |config|
  config.app_name                = "Djproject"
  config.default_version = '1.0'
  config.api_base_url["v1"] = "/api/v1"
  config.doc_base_url            = "/apipie"
  config.copyright               = "&copy; 2017 djmusic"
  # config.markup = Apipie::Markup::Markdown.new

  # where is your API defined?
  # config.api_controllers_matcher = "#{Rails.root}/app/controllers/**/*.rb"
  # config.api_controllers_matcher = File.join(Rails.root, "app", "controllers", "api", "v1","*.rb")
  # config.api_controllers_matcher = "#{Rails.root}/app/controllers/api/v1/*.rb"

  config.api_controllers_matcher = "#{Rails.root}/app/controllers/**/*/*.rb"
  config.api_routes = Rails.application.routes
  config.validate = false

end

