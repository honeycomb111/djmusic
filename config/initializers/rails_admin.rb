RailsAdmin.config do |config|
  config.authorize_with do
    authenticate_or_request_with_http_basic('Site Message') do |username, password|
      (username == 'Ryan' && password == 'DjMaps123!') or (  username == 'twosuntraders@gmail.com' && password == 'DjMaps123!')
    
    end
  end
  ### Popular gems integration

  ## == Devise ==
  # config.authenticate_with do
  #   warden.authenticate! scope: :user
  # end
  # config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end
 
  config.model Event do
    weight 3

     list do
       field :id
       field :title
       field :shortDescription
       field :longDescription
       field :city_name do 
         sortable :city_id
       end
     end
    
    show do
      field :latitude do
        label do
          "Map"
        end
        pretty_value do
          "<iframe src = 'https://maps.google.com/maps?q=#{bindings[:object].latitude},#{bindings[:object].longitude}&hl=es;z=14&amp;output=embed'></iframe>".html_safe
        end

      end
      field :ticketPrice do
        label do
         "Ticket Price(USD)" # Change the label of this field
        end
      end
      
      include_all_fields
      exclude_fields :longitude      
    end
    edit do
    #       t.string "title"
    # t.text "longDescription"
    # t.text "shortDescription"
    # t.float "latitude"
    # t.float "longitude"
    # t.integer "restrictAge"
    # t.integer "attendingCount"
    # t.integer "interestedCount"
    # t.integer "isCanceled"
    # t.integer "isFree"
    # t.string "fbLink"
    # t.integer "city_id"
    # t.datetime "created_at", null: false
    # t.datetime "updated_at", null: false
    # t.integer "dj_user_id"
    # t.integer "cover_photo_id"
    # t.datetime "openingTime"
    # t.datetime "endTime"
    # t.string "hostedBy"
    # t.float "ticketPrice"
      # Title
      # Long Description
      # Lat
      # Long
      # Attending
      # Interested
      # Restricted Age
      # City
      # DJ User
      # Photo
      # Opening time
      # End time
      # Hosted by
      # Music Type
      # Vibe Type
    include_all_fields  
    field :title, :string do  
      required true 
    end
    field :longDescription, :text do  
      required true 
    end
    field :shortDescription, :text do  
      required true 
    end    
    field :latitude, :float do  
      required true 
    end
    field :longitude, :float do  
      required true 
    end
    field :attendingCount, :integer do  
      required true 
    end 
    field :interestedCount, :integer do  
      required true 
    end 
    field :openingTime, :datetime do  
      required true 
    end 
    field :endTime, :datetime do  
      required true 
    end   
    field :hostedBy, :string do  
      required true 
    end   
    # configure :city do 
    #   show
    # end
    end
    
   

   
    
  end

  config.model User do
    weight 1
    edit do
      include_all_fields
      exclude_fields :event_users, :events
    end
    exclude_fields :dj_user_id
  end
  
  config.model DjUser do
    weight 2
    #exclude_fields :chats
    #exclude_fields :vibe_types, :music_types
  end
  config.model Report do
    weight 3
  end
  
  config.model Review do
    weight 4
  end
  
  config.model TipOff do
    weight 5
  end
  config.model CoverPhoto do
    weight 6

    # create do
    #   # field  :html_link
    #   # field :web_url do
    #       # label do
    #       # "Web Url" # Change the label of this field
    #       # end
    #   end
    # end  
    
  end
  config.model Identity do
    weight 10
  end

  config.model City do
     weight 9
  end
  config.model DjType do
     weight 10
    # show do
    #     exclude_fields :dj_users
    # end
  end
  
  config.model MusicType do
    weight 11
  end
  config.model VibeType do
    weight 12
  end
  
  config.model Activity do
     weight 13
  end
  
  config.model Bar do
     weight 14
     list do
       field :id
       field :name
       field :city do 
         sortable :city_id
       end
       field :rating
       field :display_phone

     end
      edit do
        include_all_fields
        exclude_fields :city
      end
  end  
  
  config.model Chat do
     weight 15
  end   
  
  config.model Song do
     weight 16
  end   
  
  config.model Follow do
     weight 17
  end   
#   config.model Follow do
#     weight 17
#     list do 
#     #  id              :integer          not null, primary key
# #  follower_type  
# #  follower_id    
# #  followable_type 
# #  followable_id   
# #  created_at      
#     end 
  # end     
  config.excluded_models = ["Follow", "Like", "Mention","DjMusicRelation", "DjVibeRelation", "Identity","EventMusicRelation", "EventVibeRelation","BarMusicRelation","BarVibeRelation"]


  # config.model CoverPhoto do
  #   edit do

  #     # field :target_id, :hidden do
  #     #   visible true
  #     #   default_value do
  #     #     bindings[:object].target_id
  #     #   end
  #     # end    

  #     # field :target_id do
  #     #   # This hides the field label
  #     #   label :hidden => true
  #     #   # This hides the help field *yuk*
  #     #   help ""
  #     #   def value 
  #     #     bindings[:object].target_id = User.maximum(:id).next 
  #     #   end
  #     #   # This hides the field input 
  #     #   view_helper do
  #     #     :hidden_field
  #     #   end
  #     # end


  #     include_all_fields
  #     # exclude_fields :target_type, :target
      
  #   end
  # end
end
