Rails.application.routes.draw do

  apipie
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  namespace :api do
	  namespace :v1 do
	    devise_for :users,
		         controllers: {
		             :registrations => "api/v1/users",
		             :sessions => "api/v1/sessions",
		             :passwords => "api/v1/passwords"
		         }
		  resources :users, only: [:index, :show,:update,:my_activities] do
		  	# get 'show' => "users#show"
		  	# get 'index' => "users#show"
		  	put 'update' => "users#update"
		    # get 'details' => "users#details"
				collection do
        	get 'my_activities' => 'users#my_activities'
          get 'chat_users' => "users#chat_users"

        end
      end
      resources :identities, only: :create
		  resources :city, only: :index
		  resources :vibe_types, only: :index
		  resources :dj_types, only: :index
		  resources :music_types, only: :index
		  resources :bars, only: :index

      resources :songs do
        collection do
          get 'request_songs/:event_id/:dj_user_id' => "songs#request_songs"
          post 'change_status' => 'songs#change_status'
        end
      end
		  
		  resources :chats do
        collection do
          get 'read/:reciver_id' => "chats#read"
        end
      end 
		  resources :cover_photos
		  resources :dj_users do 
		    member do
		    	post 'tip'
		    	get  'received_tips'
		    	post 'reviews' => "dj_users#post_reviews"
		    	get 'reviews' => "dj_users#get_reviews"
		    	get 'my_activities' => 'dj_users#my_activities'
          # get 'chat_users' => "dj_users#chat_users"
		    end
		   end
		  resources :events	do
        collection do
          get 'fb_events'
          get 'favorite'  => "events#get_favorite"
        end
        member do
        	post 'favorite' => "events#favorite"
        	delete 'favorite' => "events#unfavorite"
        end
        
      end
		  resources :followers, only: [] do
		  	collection do 
		  	  post  '/' => "followers#follow"
		  		# post 'follow/:dj_user_id' => "followers#follow"
		  		# post 'unfollow/:dj_user_id' => "followers#unfollow"
		  		get 'all_followees' => "followers#all_followees"
		  		get 'all_followers/:dj_user_id' => "followers#all_followers"
		  		get 'followees/:user_id' => "followers#followees"
		  	end
		  end	  
	  end
	end

  namespace :admin do
    # resources :cover_photos
    resources :users
    resources :dj_users
    root to: "users#index"
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
