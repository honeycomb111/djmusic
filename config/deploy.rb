# config valid only for current version of Capistrano
lock "3.10.0"

set :application, 'djproject'
set :repo_url, 'git@bitbucket.org:honeycomb111/djmusic.git' # Edit this to match your repository
set :branch, :master
set :deploy_to, '/home/ubuntu/djproject/'
set :pty, true
set :linked_files, %w{config/database.yml config/application.yml}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/uploads public/coverphotos}
set :keep_releases, 5
# set :rvm_custom_path, '/usr/share/rvm/bin/r'
set :rvm_type, :ubuntu
# set :rvm_ruby_version, 'jruby-1.7.19' # Edit this if you are using MRI Ruby

set :rbenv_map_bins, %w( puma pumactl )

set :puma_rackup, -> { File.join(current_path, 'config.ru') }
set :puma_state, "#{shared_path}/tmp/pids/puma.state"
set :puma_pid, "#{shared_path}/tmp/pids/puma.pid"
set :puma_bind, "unix://#{shared_path}/tmp/sockets/puma.sock"    #accept array for multi-bind
set :puma_conf, "#{shared_path}/puma.rb"
set :puma_access_log, "#{shared_path}/log/puma_error.log"
set :puma_error_log, "#{shared_path}/log/puma_access.log"
set :puma_role, :app
set :puma_env, fetch(:rack_env, fetch(:rails_env, 'production'))
set :puma_threads, [4, 8]
set :puma_workers, 0
set :puma_worker_timeout, nil
set :puma_init_active_record, true
set :puma_preload_app, false

set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }
